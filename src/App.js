import React, { useEffect } from 'react'
import { useSelector } from 'react-redux'
import { Spinner } from 'reactstrap'
import { toast, ToastContainer } from 'react-toastify'

import AppRouter from './AppRouter'
import { curUserSelector } from './customHooks/selectors/useUserSelector'
import {
    alertSelector,
    loadingSelector,
} from './customHooks/selectors/useAppSelector'
import styles from './App.module.scss'

const App = () => {
    const user = useSelector(curUserSelector)
    const alert = useSelector(alertSelector)
    const loading = useSelector(loadingSelector)

    useEffect(() => {
        if (alert) {
            toast(alert.text, {
                type: alert.type,
                position: toast.POSITION.BOTTOM_CENTER,
            })
        }
    }, [alert])

    return (
        <div className={styles.container}>
            {loading ? (
                <Spinner type="grow" color="primary" />
            ) : (
                <AppRouter user={user} />
            )}
            <ToastContainer
                autoClose={3000}
                hideProgressBar={false}
                newestOnTop={false}
                closeOnClick
                rtl={false}
                pauseOnFocusLoss
                draggable
                pauseOnHover
            />
        </div>
    )
}

export default App

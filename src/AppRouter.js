import React from 'react'
import {
    Route,
    Switch,
    Redirect,
    useLocation,
    withRouter,
} from 'react-router-dom'
import { animated, useTransition } from 'react-spring'

import { privateRoutes, publicRoutes } from './routes'

const AppRouter = ({ user }) => {
    let location = useLocation()

    const transitions = useTransition(location, {
        from: {
            opacity: 0,
            position: 'absolute',
            width: '100%',
        },
        enter: { opacity: 1 },
        leave: {
            opacity: 0,
        },
    })

    if (user) {
        return transitions((props, item) => (
            <animated.div style={props}>
                <Switch location={item}>
                    {privateRoutes.map(({ path, Component }) => (
                        <Route
                            key={path}
                            path={path}
                            component={Component}
                            exact={true}
                        />
                    ))}
                    <Redirect to={'/'} />
                </Switch>
            </animated.div>
        ))
    } else {
        return transitions((props, item) => (
            <animated.div style={props}>
                <Switch location={item}>
                    {publicRoutes.map(({ path, Component }) => (
                        <Route
                            key={path}
                            path={path}
                            component={Component}
                            exact={true}
                        />
                    ))}
                    <Redirect to={'/login'} />
                </Switch>
            </animated.div>
        ))
    }
}

export default withRouter(AppRouter)

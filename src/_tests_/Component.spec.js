import React from 'react'
import { Chats } from '../pages/Chats/Chats'
import { shallow } from 'enzyme'

describe('Chats component test', () => {
    it('should render component Chats', () => {
        const wrapper = shallow(<Chats />)
        expect(wrapper.find('h4')).toHaveLength(1)
    })
})

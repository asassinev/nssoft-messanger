import React from 'react'
import InfiniteScroll from 'react-infinite-scroller'
import { Spin } from 'antd'

import { useDialoguesHook } from '../../../customHooks/dialoguesHooks/useDialoguesHook'
import styles from './Dialogues.module.scss'

export const Dialogues = () => {
    const {
        loading,
        dialogues,
        setDialog,
        dialog,
        currentTabItem,
        counterDialogues,
        hasMore,
        counter,
        loadMore,
    } = useDialoguesHook()

    return (
        <Spin spinning={loading} type="grow" color="secondary">
            <div className={styles.container}>
                {currentTabItem === 'Waiting' && (
                    <p>Клиентов в очереди: {counterDialogues}</p>
                )}
                <div className={styles.dialogues}>
                    <InfiniteScroll
                        hasMore={hasMore}
                        loadMore={() => loadMore(counter)}
                        loader={<p key={0}>Loading...</p>}
                        useWindow={false}
                        initialLoad={false}
                    >
                        {dialogues &&
                            dialogues.map((item, index) => (
                                <div
                                    onClick={() => setDialog(item.id)}
                                    key={index}
                                    className={styles.dialog}
                                >
                                    <div
                                        className={
                                            dialog && dialog.id === item.id
                                                ? `${styles.dialogInner} ${styles.dialogActive}`
                                                : styles.dialogInner
                                        }
                                    >
                                        <div className={styles.messageUserImg}>
                                            {(item.userImg && (
                                                <img
                                                    className={
                                                        styles.messageUserImg
                                                    }
                                                    src={item.userImg}
                                                    alt={item.userName}
                                                />
                                            )) || (
                                                <p
                                                    className={`${styles.messageName} ${styles.messageUserName}`}
                                                >
                                                    {item.userName[0]}
                                                </p>
                                            )}
                                        </div>
                                        <div className={styles.dialogText}>
                                            <p>{item.userName}</p>
                                            <p className={styles.dialogMessage}>
                                                {(item.messages &&
                                                    item.messages[
                                                        item.messages.length - 1
                                                    ].content) ||
                                                    item.topic ||
                                                    'Вложение'}
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            ))}
                    </InfiniteScroll>
                </div>
            </div>
        </Spin>
    )
}

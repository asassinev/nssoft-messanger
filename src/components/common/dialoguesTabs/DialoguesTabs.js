import React from 'react'
import { Col, Row, TabContent, TabPane } from 'reactstrap'
import { useDispatch, useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faArchive,
    faClipboardCheck,
    faClock,
    faEdit,
} from '@fortawesome/free-solid-svg-icons'

import { Dialogues } from '..'
import { setCurrentTabItem } from '../../../pages/Chats/effects/dialogs/action'
import { clearCurrentDialog } from '../../../pages/Chats/effects/dialog/action'
import { currentTabSelector } from '../../../customHooks/selectors/useDialoguesSelector'
import styles from './DialoguesTabs.module.scss'

export const DialoguesTabs = () => {
    const currentTabItem = useSelector(currentTabSelector)
    const dispatch = useDispatch()

    const handleChangeTabItem = (status) => {
        dispatch(setCurrentTabItem(status))
        dispatch(clearCurrentDialog())
    }

    const tabList = [
        { status: 'Waiting', icon: faClock },
        { status: 'Active', icon: faEdit },
        { status: 'Done', icon: faClipboardCheck },
        { status: 'Saved', icon: faArchive },
    ]

    return (
        <div className={styles.container}>
            <Row className={styles.nav}>
                {tabList.map((item, index) => {
                    return (
                        <Col
                            key={index}
                            xs={3}
                            onClick={() => handleChangeTabItem(item.status)}
                            title={item.status}
                        >
                            <FontAwesomeIcon
                                className={
                                    item.status === currentTabItem
                                        ? `${styles.navItem} ${styles.navItemActive}`
                                        : styles.navItem
                                }
                                icon={item.icon}
                            />
                        </Col>
                    )
                })}
            </Row>
            <TabContent activeTab={currentTabItem}>
                <TabPane tabId="Waiting">
                    <Dialogues />
                </TabPane>
                <TabPane tabId="Active">
                    <Dialogues />
                </TabPane>
                <TabPane tabId="Done">
                    <Dialogues />
                </TabPane>
                <TabPane tabId="Saved">
                    <Dialogues />
                </TabPane>
            </TabContent>
        </div>
    )
}

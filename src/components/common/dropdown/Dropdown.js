import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faEllipsisV,
    faSignOutAlt,
    faCommentDots,
    faUser,
} from '@fortawesome/free-solid-svg-icons'
import { useDispatch } from 'react-redux'
import Modal from 'react-modal'
import {
    Dropdown,
    DropdownToggle,
    DropdownMenu,
    DropdownItem,
} from 'reactstrap'

import { userLogout } from '../../../pages/Login/effects/actions'
import { Profile } from '../../forms/profile/Profile'
import { DialoguesSettings } from '../../forms/dialoguesSettings/DialoguesSettings'
import styles from './Dropdown.module.scss'

export const DropDown = () => {
    const [dropdownOpen, setDropdownOpen] = useState(false)
    const toggle = () => setDropdownOpen((prevState) => !prevState)

    const dispatch = useDispatch()
    const [modalIsOpen, setIsOpen] = useState(false)
    const [currentModal, setCurrentModal] = useState(null)

    function openModal(nameModal) {
        setCurrentModal(nameModal)
        setIsOpen(true)
    }

    function closeModal() {
        setIsOpen(false)
    }

    return (
        <>
            <Dropdown isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle className={styles.dropdownToggle}>
                    <FontAwesomeIcon icon={faEllipsisV} />
                </DropdownToggle>
                <DropdownMenu>
                    <DropdownItem
                        className={styles.dropdownItem}
                        onClick={() => openModal('profile')}
                    >
                        <FontAwesomeIcon icon={faUser} /> <span>Профиль</span>
                    </DropdownItem>
                    <DropdownItem
                        className={styles.dropdownItem}
                        onClick={() => openModal('dialoguesSettings')}
                    >
                        <FontAwesomeIcon icon={faCommentDots} />
                        <span>Настройки диалогов</span>
                    </DropdownItem>
                    <DropdownItem
                        className={styles.dropdownItem}
                        onClick={() => dispatch(userLogout())}
                    >
                        <FontAwesomeIcon icon={faSignOutAlt} />{' '}
                        <span>Выйти</span>
                    </DropdownItem>
                </DropdownMenu>
            </Dropdown>
            <Modal
                style={{
                    content: {
                        width: 'fit-content',
                        margin: '0 auto',
                        padding: 0,
                    },
                }}
                isOpen={modalIsOpen}
                onRequestClose={closeModal}
            >
                {currentModal === 'profile' ? (
                    <Profile closeModal={closeModal} />
                ) : (
                    <DialoguesSettings closeModal={closeModal} />
                )}
            </Modal>
        </>
    )
}

import React from 'react'
import { Form as RtForm } from 'reactstrap'

import styles from './Form.module.scss'

export const Form = ({ title, footer, children }) => {
    return (
        <div className={styles.container}>
            <RtForm>
                <h2 className={styles.title}>{title}</h2>
                <hr />
                {children}
                <div className={styles.footer}>{footer}</div>
            </RtForm>
        </div>
    )
}

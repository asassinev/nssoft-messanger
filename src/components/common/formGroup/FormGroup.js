import React from 'react'
import {
    FormGroup as RtFormGroup,
    Label,
    Input,
    FormFeedback,
} from 'reactstrap'

import styles from './FormGroup.module.scss'

export const FormGroup = ({ title, feedback, ...props }) => {
    return (
        <div className={styles.container}>
            <RtFormGroup>
                <Label className={styles.label}>{title}</Label>
                <Input {...props} />
                <FormFeedback>{feedback}</FormFeedback>
            </RtFormGroup>
        </div>
    )
}

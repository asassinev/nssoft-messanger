import React, { useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearchPlus, faTimes } from '@fortawesome/free-solid-svg-icons'
import ReactModal from 'react-modal'

import styles from './Images.module.scss'

ReactModal.setAppElement('#root')

export const Images = ({ images }) => {
    const [currentImg, setCurrentImg] = useState(null)
    const [showModal, setShowModal] = useState(false)

    const handleIconClick = (imageSrc) => {
        setCurrentImg(imageSrc)
        setShowModal(true)
    }

    const handleCloseModal = () => {
        setShowModal(false)
        setCurrentImg(null)
    }

    return (
        <div className={styles.container}>
            <div className={styles.imagesWrapper}>
                {images &&
                    images.map((image, index) => (
                        <div key={index} className={styles.image}>
                            <img src={image} />
                            <FontAwesomeIcon
                                onClick={() => handleIconClick(image)}
                                className={styles.searchPlusIcon}
                                icon={faSearchPlus}
                            />
                        </div>
                    ))}
            </div>
            <ReactModal
                isOpen={showModal}
                onRequestClose={handleCloseModal}
                contentLabel="Modal"
            >
                <div className="text-center">
                    <FontAwesomeIcon
                        className={styles.closeModalIcon}
                        type="button"
                        onClick={handleCloseModal}
                        icon={faTimes}
                    />
                    <img
                        className={styles.modalImg}
                        src={currentImg}
                        alt="opened current img"
                    />
                </div>
            </ReactModal>
        </div>
    )
}

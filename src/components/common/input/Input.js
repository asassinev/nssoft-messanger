import React from 'react'

export const Input = ({ children, ...props }) => {
    return (
        <div className="form-floating mb-3">
            <input {...props} />
            <label>{children}</label>
        </div>
    )
}

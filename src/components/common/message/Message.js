import React from 'react'
import moment from 'moment'
import { Images } from '..'
import { useSelector } from 'react-redux'
import 'moment/locale/ru'

import { curDialogSelector } from '../../../customHooks/selectors/useDialogSelector'
import { curUserSelector } from '../../../customHooks/selectors/useUserSelector'
import styles from './Message.module.scss'

export const Message = ({ message }) => {
    const dialog = useSelector(curDialogSelector)
    const user = useSelector(curUserSelector)

    return (
        <div
            className={
                message.writtenBy === 'client'
                    ? `${styles.messageByClient} ${styles.container}`
                    : `${styles.messageByOperator} ${styles.container}`
            }
        >
            <div className={styles.messageUserImg}>
                {message.writtenBy === 'client' ? (
                    <>
                        {(dialog.userImg && (
                            <img
                                className={styles.messageUserImg}
                                src={dialog.userImg}
                                alt={dialog.userName}
                            />
                        )) || (
                            <p
                                className={`${styles.messageName} ${styles.messageUserName}`}
                            >
                                {dialog.userName[0]}
                            </p>
                        )}
                    </>
                ) : (
                    <>
                        {(user.photoURL && (
                            <img
                                className={styles.messageUserImg}
                                src={user.photoURL}
                                alt={user.displayName || user.email}
                            />
                        )) || (
                            <p
                                className={`${styles.messageName} ${styles.messageOperatorName}`}
                            >
                                {user.email[0]}
                            </p>
                        )}
                    </>
                )}
                <p>{moment(message.timestamp).format('LT')}</p>
            </div>
            <div
                className={
                    message.writtenBy === 'client'
                        ? `${styles.messageInner} ${styles.messageContentClient}`
                        : `${styles.messageInner} ${styles.messageContentOperator}`
                }
            >
                <div>{message.content && <p>{message.content}</p>}</div>
                {message.images && <Images images={message.images} />}
            </div>
        </div>
    )
}

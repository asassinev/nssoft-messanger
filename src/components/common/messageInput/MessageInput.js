import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
    faPaperclip,
    faPaperPlane,
    faSmile,
    faTimes,
} from '@fortawesome/free-solid-svg-icons'
import { useDispatch, useSelector } from 'react-redux'
import Picker from 'emoji-picker-react'
import ReactAutocomplete from 'react-autocomplete'
import { Col, Container, CustomInput, FormGroup, Label, Row } from 'reactstrap'

import { postNewMessage } from '../../../pages/Chats/effects/messages/action'
import { currentTabSelector } from '../../../customHooks/selectors/useDialoguesSelector'
import { usePubNubHook } from '../../../customHooks/messagesHooks/usePubNubHook'
import { useEmojiHook } from '../../../customHooks/messagesHooks/useEmojiHook'
import { useImagesHook } from '../../../customHooks/messagesHooks/useImagesHook'
import styles from './MessageInput.module.scss'

export const MessageInput = () => {
    const dispatch = useDispatch()
    const currentTabItem = useSelector(currentTabSelector)

    const {
        dialog,
        messageValue,
        setMessageValue,
        selectionStart,
        setSelectionStart,
        autocompleteItems,
        isOpenAutocomplete,
        setAutoComplete,
        handleInput,
        handleDisableSignal,
    } = usePubNubHook()

    const { showEmoji, setShowEmoji, onEmojiClick } = useEmojiHook(
        messageValue,
        setMessageValue,
        selectionStart
    )

    const { images, setImages, handleChangeFileInput, handleDeleteImg } =
        useImagesHook()

    const submitInput = (e) => {
        e.preventDefault()
        if (messageValue === '' && images.length < 1) {
            return
        }

        setAutoComplete(false)
        handleDisableSignal()
        dispatch(
            postNewMessage({
                dialog,
                message: messageValue,
                images: images,
                status: currentTabItem,
            })
        )

        setMessageValue('')
        setImages([])
    }

    return (
        <div className={styles.container}>
            <form onSubmit={submitInput} className={styles.inputForm}>
                <ReactAutocomplete
                    wrapperStyle={{ width: '100%' }}
                    open={isOpenAutocomplete}
                    renderInput={(props) => (
                        <input
                            className="w-100"
                            {...props}
                            onClick={(e) =>
                                setSelectionStart(
                                    e.currentTarget.selectionStart
                                )
                            }
                            type="text"
                            placeholder="Type your messages here..."
                        />
                    )}
                    items={autocompleteItems}
                    shouldItemRender={(item, value) =>
                        item.label.toLowerCase().indexOf(value.toLowerCase()) >
                        -1
                    }
                    getItemValue={(item) => item.label}
                    menuStyle={{
                        width: 'fit-content',
                        borderRadius: '3px',
                        boxShadow: '0 2px 12px rgba(0, 0, 0, 0.1)',
                        background: 'rgba(255, 255, 255, 0.9)',
                        margin: '2px 0',
                        fontSize: '90%',
                        position: 'absolute',
                        top: '-60px',
                        left: '20px',
                        overflow: 'auto',
                        maxHeight: '100%',
                    }}
                    renderItem={(item, highlighted) => (
                        <div
                            key={item.id}
                            style={{
                                width: '100%',
                                cursor: 'pointer',
                                backgroundColor: highlighted
                                    ? '#eee'
                                    : 'transparent',
                            }}
                        >
                            {item.label}
                        </div>
                    )}
                    value={messageValue}
                    onChange={(e) => handleInput(e)}
                    selectOnBlur={true}
                    onSelect={(val) => {
                        setAutoComplete(false)
                        setMessageValue(val)
                    }}
                />
                <div className={styles.buttonsWrapper}>
                    <div
                        className={styles.inputMessageSubIcon}
                        onMouseLeave={() => setShowEmoji(false)}
                    >
                        {showEmoji && (
                            <div className={styles.emojiPicker}>
                                <Picker
                                    disableAutoFocus={false}
                                    disableSearchBar={true}
                                    disableSkinTonePicker={true}
                                    onEmojiClick={onEmojiClick}
                                    native
                                    groupVisibility={{
                                        recently_used: false,
                                    }}
                                />
                            </div>
                        )}
                        <FontAwesomeIcon
                            onMouseMove={() => setShowEmoji(true)}
                            icon={faSmile}
                            onClick={() => setShowEmoji(!showEmoji)}
                        />
                    </div>
                    <div>
                        <FormGroup>
                            <Label for="exampleCustomFileBrowser">
                                <div className={styles.inputMessageSubIcon}>
                                    <FontAwesomeIcon icon={faPaperclip} />
                                </div>
                            </Label>
                            <CustomInput
                                className={styles.imagesInput}
                                multiple
                                type="file"
                                id="exampleCustomFileBrowser"
                                name="customFile"
                                onChange={handleChangeFileInput}
                            />
                        </FormGroup>
                    </div>
                    <div
                        type="submit"
                        onClick={submitInput}
                        className={styles.sendMessageButton}
                    >
                        <FontAwesomeIcon icon={faPaperPlane} />
                    </div>
                </div>
            </form>
            <Container>
                <Row>
                    {images.map((image, index) => (
                        <Col key={index}>
                            <div className={styles.image}>
                                <img src={image} alt={image} />
                                <FontAwesomeIcon
                                    type="button"
                                    onClick={() => handleDeleteImg(index)}
                                    className={styles.deleteImgButton}
                                    icon={faTimes}
                                />
                            </div>
                        </Col>
                    ))}
                </Row>
            </Container>
        </div>
    )
}

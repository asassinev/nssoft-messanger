import React from 'react'
import moment from 'moment'
import InfiniteScroll from 'react-infinite-scroller'
import { Spinner } from 'reactstrap'
import { Element } from 'react-scroll'

import { Message } from '../index'
import { useMessagesHook } from '../../../customHooks/messagesHooks/useMessagesHook'
import styles from './Messages.module.scss'
import { useSelector } from 'react-redux'
import { curDialogSelector } from '../../../customHooks/selectors/useDialogSelector'

export const Messages = () => {
    const { loading, messages, currentTabItem, hasMore, loadMore } =
        useMessagesHook()
    const dialog = useSelector(curDialogSelector)

    return (
        <div id={'ContainerElementID'} className={styles.container}>
            {loading && <Spinner type="grow" color="primary" />}
            {currentTabItem === 'Active' && (
                <>Диалог начат {moment(dialog.startedAt).fromNow()}</>
            )}
            {messages.length >= 1 && (
                <InfiniteScroll
                    hasMore={hasMore}
                    loadMore={loadMore}
                    loader={<Spinner key={0} type="grow" color="primary" />}
                    isReverse={true}
                    initialLoad={false}
                    useWindow={false}
                    threshold={200}
                >
                    {messages.map((message, index) => (
                        <Message key={index} message={message} />
                    ))}
                    <span>
                        {currentTabItem === 'Done' ? (
                            <>
                                Диалог завершен {moment(dialog.endAt).fromNow()}
                            </>
                        ) : (
                            <>
                                Последнее сообщение{' '}
                                {moment(
                                    messages[messages.length - 1].timestamp
                                ).fromNow()}
                            </>
                        )}
                    </span>
                </InfiniteScroll>
            )}
            <Element name="end" />
        </div>
    )
}

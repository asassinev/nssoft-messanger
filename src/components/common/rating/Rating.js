import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faStar } from '@fortawesome/free-solid-svg-icons'

import styles from './Rating.module.scss'

export const Rating = ({ rating }) => {
    const stars = []
    for (let i = 0; i < 5; i++) {
        i <= rating - 1 ? stars.push(true) : stars.push(false)
    }

    return (
        <span className={styles.container}>
            {stars.map((star, index) =>
                star ? (
                    <FontAwesomeIcon
                        className={styles.activeStar}
                        key={index}
                        icon={faStar}
                    />
                ) : (
                    <FontAwesomeIcon
                        className={styles.nonActiveStar}
                        key={index}
                        icon={faStar}
                    />
                )
            )}
        </span>
    )
}

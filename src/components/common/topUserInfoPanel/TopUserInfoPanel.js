import React from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSave, faTrashAlt } from '@fortawesome/free-solid-svg-icons'

import { Rating } from '..'
import {
    deleteDialog,
    saveDialog,
} from '../../../pages/Chats/effects/dialog/action'
import { curDialogSelector } from '../../../customHooks/selectors/useDialogSelector'
import { currentTabSelector } from '../../../customHooks/selectors/useDialoguesSelector'
import styles from './TopUserInfoPanel.module.scss'

export const TopUserInfoPanel = ({ setOpenUserInfo }) => {
    const dialog = useSelector(curDialogSelector)
    const currentTabItem = useSelector(currentTabSelector)
    const dispatch = useDispatch()

    const saveCurrentDialog = () => {
        dispatch(saveDialog(dialog))
    }

    const deleteCurrentDialog = () => {
        dispatch(deleteDialog(dialog))
    }

    return (
        <div className={styles.container}>
            <div>
                <p
                    className={styles.userName}
                    onClick={() => setOpenUserInfo(true)}
                >
                    {dialog.userName || 'userName'}
                </p>
                {currentTabItem === 'Done' && <Rating rating={dialog.rating} />}
            </div>
            {currentTabItem !== 'Saved' ? (
                <button
                    className={`${styles.iconButton} ${styles.saveButton}`}
                    title="Save dialog"
                    onClick={() => saveCurrentDialog()}
                >
                    <FontAwesomeIcon icon={faSave} />
                </button>
            ) : (
                <button
                    className={`${styles.iconButton} ${styles.deleteButton}`}
                    title="Delete dialog"
                    onClick={() => deleteCurrentDialog()}
                >
                    <FontAwesomeIcon icon={faTrashAlt} />
                </button>
            )}
        </div>
    )
}

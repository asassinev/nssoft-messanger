import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { useSelector } from 'react-redux'
import { faTimes } from '@fortawesome/free-solid-svg-icons/faTimes'

import { curDialogSelector } from '../../../customHooks/selectors/useDialogSelector'
import styles from './UserInfoPanel.module.scss'

export const UserInfoPanel = (props) => {
    const dialog = useSelector(curDialogSelector)
    return (
        <div
            className={
                props.openUserInfo
                    ? `${styles.container} ${styles.isOpen}`
                    : styles.container
            }
        >
            <div className={styles.infoPanel}>
                <button
                    tabIndex={-1}
                    className={styles.button}
                    onClick={() => props.onClose()}
                >
                    <FontAwesomeIcon icon={faTimes} />
                </button>
                <div className={styles.description}>
                    <p>{dialog.userImg || 'image'}</p>
                    <p>{dialog.userName}</p>
                    <p>phone</p>
                    <p>email</p>
                </div>
            </div>
        </div>
    )
}

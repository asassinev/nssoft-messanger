import React from 'react'
import { Field, FieldArray, Form, Formik } from 'formik'
import { useDispatch, useSelector } from 'react-redux'
import { Spin } from 'antd'
import { Col, Row } from 'reactstrap'

import { Button } from '../../hoc'
import { userUpdateDialoguesSettings } from '../../../pages/Login/effects/actions'
import { userSelector } from '../../../customHooks/selectors/useUserSelector'
import styles from './DialoguesSettings.module.scss'

export const DialoguesSettings = ({ closeModal }) => {
    const { user, loading, dialoguesSettings } = useSelector(userSelector)
    const dispatch = useDispatch()

    const updatedSettings = (values) => {
        let newPhrases = []
        let newListOfTopics = []
        let newListOfSubtopics = []
        let newGreeting = values.greeting
        values.phrases.map((item) =>
            item !== '' ? newPhrases.push(item) : null
        )
        values.listOfTopics.map((item) =>
            item !== '' ? newListOfTopics.push(item) : null
        )
        values.listOfSubtopics.map((item) =>
            item !== '' ? newListOfSubtopics.push(item) : null
        )

        if (newPhrases.length < 1) {
            newPhrases.push('')
        }

        if (newListOfTopics.length < 1) {
            newListOfTopics.push('')
        }

        if (newListOfSubtopics.length < 1) {
            newListOfSubtopics.push('')
        }

        const data = {
            greeting: newGreeting,
            phrases: newPhrases,
            listOfTopics: newListOfTopics,
            listOfSubtopics: newListOfSubtopics,
        }

        dispatch(userUpdateDialoguesSettings(data, user.uid))

        values.phrases = newPhrases
        values.listOfTopics = newListOfTopics
        values.listOfSubtopics = newListOfSubtopics
        values.greeting = newGreeting
    }

    return (
        <Spin spinning={loading}>
            {dialoguesSettings && (
                <Formik
                    initialValues={{
                        greeting: dialoguesSettings.greeting,
                        phrases: dialoguesSettings.phrases,
                        listOfTopics: dialoguesSettings.listOfTopics,
                        listOfSubtopics: dialoguesSettings.listOfSubtopics,
                    }}
                    onSubmit={(values, { setSubmitting }) => {
                        updatedSettings(values)
                        setSubmitting(false)
                    }}
                >
                    {({ values, handleSubmit, isSubmitting, handleChange }) => (
                        <Form
                            className={styles.container}
                            onSubmit={handleSubmit}
                        >
                            <Row>
                                <Col xs={6}>
                                    <div className={styles.formBlock}>
                                        <p>Готовые фразы:</p>
                                        <div className={styles.formField}>
                                            <FieldArray
                                                name="phrases"
                                                render={(arrayHelpers) => (
                                                    <>
                                                        {values.phrases &&
                                                            values.phrases.map(
                                                                (
                                                                    phrase,
                                                                    index
                                                                ) => (
                                                                    <div
                                                                        key={
                                                                            index
                                                                        }
                                                                    >
                                                                        <Field
                                                                            className={
                                                                                styles.formInput
                                                                            }
                                                                            name={`phrases.${index}`}
                                                                        />
                                                                        <Button
                                                                            type="button"
                                                                            onClick={() =>
                                                                                arrayHelpers.remove(
                                                                                    index
                                                                                )
                                                                            }
                                                                        >
                                                                            -
                                                                        </Button>
                                                                    </div>
                                                                )
                                                            )}
                                                        <Button
                                                            type="button"
                                                            onClick={() =>
                                                                arrayHelpers.insert(
                                                                    values
                                                                        .phrases
                                                                        .length,
                                                                    ''
                                                                )
                                                            }
                                                        >
                                                            Add a phrase
                                                        </Button>
                                                    </>
                                                )}
                                            />
                                        </div>
                                        <div className="mt-3">
                                            <p className={styles.greeting}>
                                                Автоматическое приветствие:
                                            </p>
                                            <input
                                                type="text"
                                                className={styles.formInput}
                                                name="greeting"
                                                id="greeting"
                                                onChange={handleChange}
                                                value={values.greeting}
                                            />
                                        </div>
                                    </div>
                                </Col>
                                <Col xs={6}>
                                    <div className={styles.formBlock}>
                                        <p>Список тем:</p>
                                        <div className={styles.formField}>
                                            <FieldArray
                                                name="listOfTopics"
                                                render={(arrayHelpers) => (
                                                    <>
                                                        {values.listOfTopics &&
                                                            values.listOfTopics.map(
                                                                (
                                                                    topic,
                                                                    index
                                                                ) => (
                                                                    <div
                                                                        key={
                                                                            index
                                                                        }
                                                                    >
                                                                        <Field
                                                                            className={
                                                                                styles.formInput
                                                                            }
                                                                            name={`listOfTopics.${index}`}
                                                                        />
                                                                        <Button
                                                                            type="button"
                                                                            onClick={() =>
                                                                                arrayHelpers.remove(
                                                                                    index
                                                                                )
                                                                            } // remove a friend from the list
                                                                        >
                                                                            -
                                                                        </Button>
                                                                    </div>
                                                                )
                                                            )}
                                                        <Button
                                                            type="button"
                                                            onClick={() =>
                                                                arrayHelpers.insert(
                                                                    values
                                                                        .listOfTopics
                                                                        .length,
                                                                    ''
                                                                )
                                                            }
                                                        >
                                                            Added topic
                                                        </Button>
                                                    </>
                                                )}
                                            />
                                        </div>
                                        <p>Список подтем:</p>
                                        <div className={styles.formField}>
                                            <FieldArray
                                                name="listOfSubtopics"
                                                render={(arrayHelpers) => (
                                                    <>
                                                        {values.listOfSubtopics &&
                                                            values.listOfSubtopics.map(
                                                                (
                                                                    subtopic,
                                                                    index
                                                                ) => (
                                                                    <div
                                                                        key={
                                                                            index
                                                                        }
                                                                    >
                                                                        <Field
                                                                            className={
                                                                                styles.formInput
                                                                            }
                                                                            name={`listOfSubtopics.${index}`}
                                                                        />
                                                                        <Button
                                                                            type="button"
                                                                            onClick={() =>
                                                                                arrayHelpers.remove(
                                                                                    index
                                                                                )
                                                                            } // remove a friend from the list
                                                                        >
                                                                            -
                                                                        </Button>
                                                                    </div>
                                                                )
                                                            )}
                                                        <Button
                                                            type="button"
                                                            onClick={() =>
                                                                arrayHelpers.insert(
                                                                    values
                                                                        .listOfSubtopics
                                                                        .length,
                                                                    ''
                                                                )
                                                            }
                                                        >
                                                            Added subtopic
                                                        </Button>
                                                    </>
                                                )}
                                            />
                                        </div>
                                    </div>
                                </Col>
                                <div className="text-center b-0">
                                    <Button type="button" onClick={closeModal}>
                                        Закрыть
                                    </Button>
                                    <Button
                                        type="submit"
                                        disabled={isSubmitting}
                                    >
                                        Сохранить
                                    </Button>
                                </div>
                            </Row>
                        </Form>
                    )}
                </Formik>
            )}
        </Spin>
    )
}

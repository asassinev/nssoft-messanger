import React from 'react'
import { useDispatch } from 'react-redux'
import { Formik } from 'formik'
import { Link } from 'react-router-dom'
import * as Yup from 'yup'

import { Button } from '../../hoc'
import { FormGroup, Form } from '../../common'
import { forgotPassword } from '../../../pages/Login/effects/actions'
import styles from './ForgotPassword.module.scss'

export const ForgotPassword = () => {
    const dispatch = useDispatch()
    const schema = Yup.object().shape({
        email: Yup.string()
            .required('Email обязателен.')
            .email('Email должны быть корректным.'),
    })

    return (
        <Formik
            initialValues={{ email: '' }}
            validationSchema={schema}
            onSubmit={(values, { setSubmitting }) => {
                dispatch(forgotPassword(values.email))
                setSubmitting(false)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Form
                    title={'Восстановление пароля'}
                    footer={
                        <>
                            <Button
                                type="submit"
                                onClick={handleSubmit}
                                disabled={isSubmitting}
                            >
                                Отправить
                            </Button>
                            <div className={styles.formLinks}>
                                <Link to="/login">Войти</Link>
                                <Link to="/sign-up">Регистрация</Link>
                            </div>
                        </>
                    }
                >
                    <FormGroup
                        title={'Email'}
                        feedback={touched.email && errors.email}
                        type="email"
                        name="email"
                        id="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        placeholder="name@example.com"
                        valid={
                            values.email !== '' &&
                            !(errors.email && errors.email !== '')
                        }
                        invalid={
                            touched.email && errors.email && errors.email !== ''
                        }
                    />
                </Form>
            )}
        </Formik>
    )
}

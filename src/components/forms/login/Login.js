import React from 'react'
import { useDispatch } from 'react-redux'
import { Formik } from 'formik'
import * as Yup from 'yup'
import {
    Label,
    Input,
    Row,
    Col,
    FormGroup as RtFormGroup,
    FormFeedback,
} from 'reactstrap'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { Link } from 'react-router-dom'
import { faFacebookSquare, faGoogle } from '@fortawesome/free-brands-svg-icons'
import firebase from 'firebase'

import { Button } from '../../hoc'
import { FormGroup, Form } from '../../common'
import {
    userLogin,
    userLoginWithSocial,
} from '../../../pages/Login/effects/actions'
import styles from './Login.module.scss'

export const Login = () => {
    const dispatch = useDispatch()
    const loginSchema = Yup.object().shape({
        email: Yup.string()
            .required('Email обязателен.')
            .email('Email должны быть корректным.'),
        password: Yup.string().required('Пароль обязателен.'),
    })

    const handleGoogleButton = () => {
        const provider = new firebase.auth.GoogleAuthProvider()

        dispatch(userLoginWithSocial(provider))
    }

    const handleFacebookButton = () => {
        const provider = new firebase.auth.FacebookAuthProvider()

        dispatch(userLoginWithSocial(provider))
    }

    return (
        <Formik
            initialValues={{ email: '', password: '' }}
            validationSchema={loginSchema}
            onSubmit={(values, { setSubmitting }) => {
                dispatch(userLogin(values))
                setSubmitting(false)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Form
                    title={'Авторизация'}
                    footer={
                        <>
                            <Button
                                type="submit"
                                onClick={handleSubmit}
                                disabled={isSubmitting}
                            >
                                Войти
                            </Button>
                            <div className={styles.formLinks}>
                                <Link to="/sign-up">Зарегистрироваться?</Link>
                            </div>
                        </>
                    }
                >
                    <div className={styles.formGroup}>
                        <RtFormGroup>
                            <Row>
                                <Col>
                                    <div
                                        className={`${styles.socialButton} ${styles.socialFb}`}
                                        onClick={handleFacebookButton}
                                    >
                                        <FontAwesomeIcon
                                            icon={faFacebookSquare}
                                            color={'#3b5998'}
                                        />
                                        <span> Facebook</span>
                                    </div>
                                </Col>
                                <Col>
                                    <div
                                        onClick={handleGoogleButton}
                                        className={`${styles.socialButton} ${styles.socialGoogle}`}
                                    >
                                        <FontAwesomeIcon
                                            icon={faGoogle}
                                            color={'#dc4b38'}
                                        />
                                        <span> Google</span>
                                    </div>
                                </Col>
                            </Row>
                        </RtFormGroup>
                    </div>
                    <FormGroup
                        title={'Email'}
                        feedback={touched.email && errors.email}
                        type="email"
                        name="email"
                        id="email"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                        placeholder="name@example.com"
                        valid={
                            values.email !== '' &&
                            !(errors.email && errors.email !== '')
                        }
                        invalid={
                            touched.email && errors.email && errors.email !== ''
                        }
                    />
                    <div className={styles.formGroup}>
                        <RtFormGroup>
                            <Label
                                for="password"
                                className={`${styles.formLabel} ${styles.formLabelPassword}`}
                            >
                                Пароль{' '}
                                <Link to="/forgot-password">
                                    Забыли пароль?
                                </Link>
                            </Label>
                            <Input
                                type="password"
                                name="password"
                                id="password"
                                onChange={handleChange}
                                onBlur={handleBlur}
                                value={values.password}
                                placeholder="Enter you password"
                                valid={
                                    values.password !== '' &&
                                    !(errors.password && errors.password !== '')
                                }
                                invalid={
                                    touched.password &&
                                    errors.password &&
                                    errors.password !== ''
                                }
                            />
                            <FormFeedback>
                                {touched.password && errors.password}
                            </FormFeedback>
                        </RtFormGroup>
                    </div>
                </Form>
            )}
        </Formik>
    )
}

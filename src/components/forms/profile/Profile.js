import React, { useEffect, useState } from 'react'
import { Formik } from 'formik'
import * as Yup from 'yup'
import { useDispatch, useSelector } from 'react-redux'
import { Spin } from 'antd'

import { Button } from '../../hoc'
import { Form, FormGroup } from '../../common'
import { firebaseApp } from '../../../firebaseConfig'
import {
    resetUserUpdated,
    userUpdateProfile,
} from '../../../pages/Login/effects/actions'
import { userSelector } from '../../../customHooks/selectors/useUserSelector'
import styles from './Profile.module.scss'

export const Profile = ({ closeModal }) => {
    const inputImg = React.createRef()
    const [image, setImage] = useState(null)
    const [curUser, setCurUser] = useState()
    const dispatch = useDispatch()
    const { loading, error, updated } = useSelector(userSelector)

    useEffect(() => {
        firebaseApp.auth().onAuthStateChanged((user) => {
            if (user) {
                setCurUser(user)
            }
        })
    }, [])

    useEffect(() => {
        if (!error && updated) {
            dispatch(resetUserUpdated())
            return closeModal()
        }
    }, [error, updated])

    const handleChangeFileInput = (e) => {
        e.preventDefault()
        let item = inputImg.current.files[0]
        if (item === undefined) return
        if (item.type.indexOf('image') === 0) {
            let reader = new FileReader()
            reader.onload = function (item) {
                setImage(item.currentTarget.result)
            }
            reader.readAsDataURL(item)
        }
    }
    const profileSchema = Yup.object().shape({
        email: Yup.string().email('Invalid email address!').required(),
        name: Yup.string(),
        password: Yup.string()
            .min(8, 'Минимальная длина пароля не менее 8.')
            .matches(
                /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]/,
                'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков.'
            ),
        confirmPassword: Yup.string().test(
            'passwords-match',
            'Passwords must match',
            function (value) {
                return this.parent.password === value
            }
        ),
    })

    return (
        <>
            {curUser && (
                <Formik
                    initialValues={{
                        email: curUser.email || '',
                        name: curUser.displayName || '',
                        password: '',
                        confirmPassword: '',
                    }}
                    validationSchema={profileSchema}
                    onSubmit={(values, { setSubmitting }) => {
                        dispatch(userUpdateProfile(values, image))
                        setSubmitting(false)
                        values.password = ''
                        values.confirmPassword = ''
                    }}
                >
                    {({
                        values,
                        errors,
                        touched,
                        handleChange,
                        handleBlur,
                        handleSubmit,
                        isSubmitting,
                    }) => (
                        <Spin spinning={loading}>
                            <Form
                                title={'Профиль'}
                                footer={
                                    <>
                                        <Button
                                            type="button"
                                            onClick={closeModal}
                                        >
                                            Закрыть
                                        </Button>
                                        <Button
                                            type="submit"
                                            onClick={handleSubmit}
                                            disabled={isSubmitting}
                                        >
                                            Сохранить
                                        </Button>
                                    </>
                                }
                            >
                                <div className={styles.userImageWrapper}>
                                    {curUser.photoURL !== null ||
                                    image !== null ? (
                                        <img
                                            className={styles.userImage}
                                            src={image || curUser.photoURL}
                                            alt="userImg"
                                        />
                                    ) : (
                                        <p className={styles.userImageAlt}>
                                            {(curUser.displayName &&
                                                curUser.displayName[0]) ||
                                                curUser.email[0]}
                                        </p>
                                    )}
                                    <label>
                                        <div className={styles.userImageButton}>
                                            Загрузить фото
                                        </div>
                                        <input
                                            onChange={handleChangeFileInput}
                                            value={values.photo}
                                            multiple
                                            ref={inputImg}
                                            type="file"
                                            style={{ display: 'none' }}
                                        />
                                    </label>
                                </div>
                                <hr />
                                <FormGroup
                                    title={'Email'}
                                    feedback={touched.email && errors.email}
                                    type="email"
                                    name="email"
                                    id="email"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.email}
                                    placeholder="name@example.com"
                                    valid={
                                        values.email !== '' &&
                                        !(errors.email && errors.email !== '')
                                    }
                                    invalid={
                                        touched.email &&
                                        errors.email &&
                                        errors.email !== ''
                                    }
                                />

                                <FormGroup
                                    title={'Имя пользователя'}
                                    feedback={touched.name && errors.name}
                                    type="name"
                                    name="name"
                                    id="name"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.name}
                                    placeholder="имя"
                                    valid={
                                        values.name !== '' &&
                                        !(errors.name && errors.name !== '')
                                    }
                                    invalid={
                                        touched.name &&
                                        errors.name &&
                                        errors.name !== ''
                                    }
                                />

                                <FormGroup
                                    title={'Пароль'}
                                    feedback={
                                        touched.password && errors.password
                                    }
                                    type="password"
                                    name="password"
                                    id="Password"
                                    placeholder="Password"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.password}
                                    valid={
                                        values.password !== '' &&
                                        !(
                                            errors.password &&
                                            errors.password !== ''
                                        )
                                    }
                                    invalid={
                                        touched.password &&
                                        errors.password &&
                                        errors.password !== ''
                                    }
                                />
                                <FormGroup
                                    title={'Подтверждение пароля'}
                                    feedback={
                                        touched.confirmPassword &&
                                        errors.confirmPassword
                                    }
                                    type="password"
                                    name="confirmPassword"
                                    id="confirmPassword"
                                    placeholder="confirmPassword"
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                    value={values.confirmPassword}
                                    valid={
                                        values.confirmPassword !== '' &&
                                        !(
                                            errors.confirmPassword &&
                                            errors.confirmPassword !== ''
                                        )
                                    }
                                    invalid={
                                        touched.confirmPassword &&
                                        errors.confirmPassword &&
                                        errors.confirmPassword !== ''
                                    }
                                />
                            </Form>
                        </Spin>
                    )}
                </Formik>
            )}
        </>
    )
}

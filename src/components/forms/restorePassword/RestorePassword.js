import React, { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import * as Yup from 'yup'
import { Formik } from 'formik'
import { Link, useHistory, useLocation } from 'react-router-dom'

import { Button } from '../../hoc'
import { Form, FormGroup } from '../../common'
import { resetPassword } from '../../../pages/Login/effects/actions'
import styles from './RestorePassword.module.scss'
import { alertSelector } from '../../../customHooks/selectors/useAppSelector'

export const RestorePassword = () => {
    const alert = useSelector(alertSelector)
    const dispatch = useDispatch()
    let history = useHistory()
    let location = useLocation()

    const scheme = Yup.object().shape({
        password: Yup.string()
            .min(8, 'Минимальная длина пароля не менее 8.')
            .matches(
                /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]/,
                'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков.'
            )
            .required('Пароль обязателен.'),
        confirmPassword: Yup.string()
            .required('Пароль обязателен.')
            .test(
                'passwords-match',
                'Пароли должны совпадать.',
                function (value) {
                    return this.parent.password === value
                }
            ),
    })

    useEffect(() => {
        if (alert && alert.type === 'success') {
            history.push('/login')
        }
    }, [alert])

    return (
        <Formik
            initialValues={{
                password: '',
                confirmPassword: '',
            }}
            validationSchema={scheme}
            onSubmit={(values, { setSubmitting }) => {
                dispatch(
                    resetPassword(
                        location.search.substring(28, 82),
                        values.password
                    )
                )
                setSubmitting(false)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Form
                    title={'Обновление пароля'}
                    footer={
                        <>
                            <Button
                                type="submit"
                                onClick={handleSubmit}
                                disabled={isSubmitting}
                            >
                                Сохранить
                            </Button>
                            <div className={styles.formLinks}>
                                <Link to="/login">Войти</Link>
                                <Link to="/sign-up">Зарегистрироваться</Link>
                            </div>
                        </>
                    }
                >
                    <FormGroup
                        title={'Пароль'}
                        feedback={touched.password && errors.password}
                        type="password"
                        name="password"
                        id="Password"
                        placeholder="Password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        valid={
                            values.password !== '' &&
                            !(errors.password && errors.password !== '')
                        }
                        invalid={
                            touched.password &&
                            errors.password &&
                            errors.password !== ''
                        }
                    />
                    <FormGroup
                        title={'Подтверждение пароля'}
                        feedback={
                            touched.confirmPassword && errors.confirmPassword
                        }
                        type="password"
                        name="confirmPassword"
                        id="confirmPassword"
                        placeholder="confirmPassword"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.confirmPassword}
                        valid={
                            values.confirmPassword !== '' &&
                            !(
                                errors.confirmPassword &&
                                errors.confirmPassword !== ''
                            )
                        }
                        invalid={
                            touched.confirmPassword &&
                            errors.confirmPassword &&
                            errors.confirmPassword !== ''
                        }
                    />
                </Form>
            )}
        </Formik>
    )
}

import React from 'react'
import { useDispatch } from 'react-redux'
import { Link } from 'react-router-dom'
import { Formik } from 'formik'
import * as Yup from 'yup'

import { Button } from '../../hoc'
import { Form, FormGroup } from '../../common'
import { signUp } from '../../../pages/Login/effects/actions'
import styles from './SignUp.module.scss'

export const SignUp = () => {
    const dispatch = useDispatch()

    const signUpForm = Yup.object().shape({
        email: Yup.string()
            .email('Email должны быть корректным.')
            .required('Email обязателен.'),
        password: Yup.string()
            .min(8, 'Минимальная длина пароля не менее 8.')
            .matches(
                /(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])[0-9a-zA-Z!@#$%^&*]/,
                'Пароль должен содержать цифру, буквы в нижнем и верхнем регистре и иметь длину не менее 8 знаков.'
            )
            .required('Пароль обязателен.'),
        confirmPassword: Yup.string()
            .required('Пароль обязателен.')
            .test(
                'passwords-match',
                'Пароли должны совпадать.',
                function (value) {
                    return this.parent.password === value
                }
            ),
    })
    return (
        <Formik
            initialValues={{
                email: '',
                password: '',
                confirmPassword: '',
            }}
            validationSchema={signUpForm}
            onSubmit={(values, { setSubmitting }) => {
                dispatch(signUp(values))
                setSubmitting(false)
            }}
        >
            {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
            }) => (
                <Form
                    title={'Регистрация'}
                    footer={
                        <>
                            <Button
                                type="submit"
                                onClick={handleSubmit}
                                disabled={isSubmitting}
                            >
                                Зарегистрироваться
                            </Button>
                            <div className={styles.formLinks}>
                                <Link to="/login">Авторизоваться</Link>
                                <Link to="/forgot-password">
                                    Забыли пароль?
                                </Link>
                            </div>
                        </>
                    }
                >
                    <FormGroup
                        title={'Email'}
                        feedback={touched.email && errors.email}
                        type="email"
                        name="email"
                        valid={
                            values.email !== '' &&
                            !(errors.email && errors.email !== '')
                        }
                        invalid={
                            touched.email && errors.email && errors.email !== ''
                        }
                        id="email"
                        placeholder="email@example.com"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.email}
                    />
                    <FormGroup
                        title={'Пароль'}
                        feedback={touched.password && errors.password}
                        type="password"
                        name="password"
                        id="Password"
                        placeholder="Password"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.password}
                        valid={
                            values.password !== '' &&
                            !(errors.password && errors.password !== '')
                        }
                        invalid={
                            touched.password &&
                            errors.password &&
                            errors.password !== ''
                        }
                    />
                    <FormGroup
                        title={'Подтверждение пароля'}
                        feedback={
                            touched.confirmPassword && errors.confirmPassword
                        }
                        type="password"
                        name="confirmPassword"
                        id="confirmPassword"
                        placeholder="confirmPassword"
                        onChange={handleChange}
                        onBlur={handleBlur}
                        value={values.confirmPassword}
                        valid={
                            values.confirmPassword !== '' &&
                            !(
                                errors.confirmPassword &&
                                errors.confirmPassword !== ''
                            )
                        }
                        invalid={
                            touched.confirmPassword &&
                            errors.confirmPassword &&
                            errors.confirmPassword !== ''
                        }
                    />
                </Form>
            )}
        </Formik>
    )
}

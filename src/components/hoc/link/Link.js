import React from 'react'
import { Link as LinkRouter } from 'react-router-dom'

import styles from './Link.module.scss'

export const Link = ({ children, ...props }) => {
    return (
        <LinkRouter className={styles.container} {...props}>
            {children}
        </LinkRouter>
    )
}

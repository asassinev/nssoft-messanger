import { useEffect, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'

import { dialoguesSelector } from '../selectors/useDialoguesSelector'
import { dialogSelector } from '../selectors/useDialogSelector'
import { firebaseApp, firebaseUrl } from '../../firebaseConfig'
import {
    setDbRef,
    setDialogues,
} from '../../pages/Chats/effects/dialogs/action'
import {
    clearCurrentDialog,
    clearDialogId,
    setCurrentDialog,
} from '../../pages/Chats/effects/dialog/action'

export const useDialoguesHook = () => {
    const { dialogues, currentTabItem, searchValue, dbRef } =
        useSelector(dialoguesSelector)
    const { dialog, dialogId } = useSelector(dialogSelector)
    const dispatch = useDispatch()
    const [hasMore, setHasMore] = useState(false)
    const [counterDialogues, setCounterDialogues] = useState(0)
    const [counter, setCounter] = useState(15)
    const [loading, setLoading] = useState(false)

    const loadMore = (count) => {
        if (typeof dbRef !== 'string' && dbRef !== null) {
            dbRef.off()
        }
        setLoading(true)

        const ref = firebaseApp
            .database(firebaseUrl)
            .ref(currentTabItem)
            .orderByKey()
            .limitToLast(count)

        ref.on('value', (snapshot) => {
            if (snapshot.val() === null) {
                dispatch(setDialogues([]))
                setLoading(false)

                return
            }

            let arrayOfKeys = Object.keys(snapshot.val())
            let results = arrayOfKeys.map((id) => {
                return { id, ...snapshot.val()[id] }
            })

            if (searchValue !== '' && searchValue !== undefined) {
                results = results.filter((item) =>
                    item.userName
                        .toLowerCase()
                        .includes(searchValue.toLowerCase()) ||
                    item.messages.filter((m) =>
                        m.content
                            .toLowerCase()
                            .includes(searchValue.toLowerCase())
                    ).length > 0
                        ? item
                        : null
                )
            }

            if (results.length > count - 1) {
                let newCounter = count
                newCounter += 15
                setCounter(newCounter)
                setHasMore(true)
            } else {
                setHasMore(false)
            }
            dispatch(setDialogues(results.reverse()))
            setLoading(false)
        })
        dispatch(setDbRef(ref))
    }

    const loadCounterDialogues = () => {
        setLoading(true)

        firebaseApp
            .database(firebaseUrl)
            .ref(currentTabItem)
            .on('value', (snapshot) => {
                if (snapshot.val() === null) {
                    setCounterDialogues(0)
                    return
                }
                let arrayOfKeys = Object.keys(snapshot.val())
                setCounterDialogues(arrayOfKeys.length)
                setLoading(false)
            })
    }

    useEffect(() => {
        loadCounterDialogues()
        loadMore(15)
    }, [currentTabItem])

    useEffect(() => {
        loadMore(counter)
    }, [searchValue])

    const escFunction = (e) => {
        if (e.keyCode === 27) {
            dispatch(clearCurrentDialog())
        }
    }

    useEffect(() => {
        document.addEventListener('keydown', escFunction, false)
        return () => {
            document.removeEventListener('keydown', escFunction, false)
        }
    }, [])

    useEffect(() => {
        if (dialogId !== null) {
            setDialog(dialogId)
            dispatch(clearDialogId())
        } else if (dialog) {
            setDialog(dialog.id)
        }
    }, [dialogues])

    const setDialog = (id) => {
        const dialog = dialogues.filter((dialog) =>
            dialog.id === id ? dialog : null
        )
        if (dialog.length === 0) {
            dispatch(clearCurrentDialog())
        } else {
            dispatch(setCurrentDialog(dialog[0]))
        }
    }

    return {
        loading,
        dialogues,
        setDialog,
        dialog,
        currentTabItem,
        counterDialogues,
        hasMore,
        counter,
        loadMore,
    }
}

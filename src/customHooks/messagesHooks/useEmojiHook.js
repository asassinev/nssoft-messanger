import { useState } from 'react'

export const useEmojiHook = (messageValue, setMessageValue, selectionStart) => {
    const [showEmoji, setShowEmoji] = useState(false)

    const onEmojiClick = (event, emojiObject) => {
        let startMessage = messageValue.substring(0, selectionStart)
        let endMessage =
            emojiObject.emoji + messageValue.substring(selectionStart)

        setMessageValue(startMessage + endMessage)
    }

    return { showEmoji, setShowEmoji, onEmojiClick }
}

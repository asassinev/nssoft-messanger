import { useEffect, useState } from 'react'

export const useImagesHook = () => {
    const [images, setImages] = useState([])

    const handleChangeFileInput = (e) => {
        e.preventDefault()

        let item = e.target.files[0]
        if (item.type.indexOf('image') === 0) {
            let reader = new FileReader()
            reader.onload = function (item) {
                const newImages = images
                newImages.push(item.currentTarget.result)
                setImages([...newImages])
            }
            reader.readAsDataURL(item)
        }
    }

    const handleDeleteImg = (number) => {
        setImages(images.filter((p, index) => index !== number))
    }

    const pasteImages = (e) => {
        let item = e.clipboardData.items[0]
        if (item.type.indexOf('image') === 0) {
            let blob = item.getAsFile()
            let reader = new FileReader()

            reader.onload = function (event) {
                setImages([...images, event.target.result])
            }

            reader.readAsDataURL(blob)
        }
    }

    useEffect(() => {
        document.addEventListener('paste', pasteImages, false)

        return () => {
            document.removeEventListener('paste', pasteImages, false)
        }
    }, [])

    return {
        images,
        setImages,
        handleChangeFileInput,
        handleDeleteImg,
    }
}

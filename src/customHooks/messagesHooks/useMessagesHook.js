import { useDispatch, useSelector } from 'react-redux'
import { useEffect, useState } from 'react'
import { scroller } from 'react-scroll'

import { currentTabSelector } from '../selectors/useDialoguesSelector'
import { curDialogSelector } from '../selectors/useDialogSelector'
import { messagesSelector } from '../selectors/useMessagesSelector'
import { firebaseApp, firebaseUrl } from '../../firebaseConfig'
import { setMessages } from '../../pages/Chats/effects/messages/action'

export const useMessagesHook = () => {
    const currentTabItem = useSelector(currentTabSelector)
    const dialog = useSelector(curDialogSelector)
    const messages = useSelector(messagesSelector)
    const dispatch = useDispatch()
    const [hasMore, setHasMore] = useState(false)
    const [counter, setCounter] = useState(null)
    const [loading, setLoading] = useState(false)
    const [DBRef, setDBRef] = useState(null)

    useEffect(() => {
        setCounter(15)
    }, [])

    useEffect(() => {
        if (!counter) {
            return
        }
        if (DBRef !== null) {
            DBRef.off()
        }
        setDBRef(null)
        setLoading(true)
        let dbRef = firebaseApp
            .database(firebaseUrl)
            .ref(currentTabItem)
            .child(dialog.id + '/messages')
            .limitToLast(counter)
        setDBRef(dbRef)
    }, [dialog, counter])

    useEffect(() => {
        if (DBRef !== null) {
            loadMessages()
        }
        return () => {
            if (DBRef) {
                DBRef.off()
                setDBRef(null)
            }
        }
    }, [DBRef])

    const loadMessages = () => {
        DBRef.on('value', (snapshot) => {
            if (snapshot.val() === null) {
                setLoading(false)

                return dispatch(setMessages([]))
            }
            let arrayOfKeys = Object.keys(snapshot.val())
            let results = arrayOfKeys.map((key) => snapshot.val()[key])
            if (results.length > counter - 1) {
                setHasMore(true)
            } else {
                setHasMore(false)
            }
            dispatch(setMessages(results))
            setLoading(false)
        })
    }

    const loadMore = () => {
        setCounter(counter + 15)
    }

    useEffect(() => {
        if (counter === 15 && messages.length <= 15 && messages.length !== 0) {
            scroller.scrollTo('end', { containerId: 'ContainerElementID' })
        }
    }, [messages, counter])

    return { loading, messages, hasMore, loadMore, currentTabItem }
}

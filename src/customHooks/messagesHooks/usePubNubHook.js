import { useEffect, useState } from 'react'
import { usePubNub } from 'pubnub-react'
import { useDispatch, useSelector } from 'react-redux'

import { dialogSelector } from '../selectors/useDialogSelector'
import {
    disableSignal,
    enableSignal,
} from '../../pages/Chats/effects/messages/action'
import { dialoguesSettingsSelector } from '../selectors/useUserSelector'

export const usePubNubHook = () => {
    const { dialog, channels } = useSelector(dialogSelector)
    const dialoguesSettings = useSelector(dialoguesSettingsSelector)
    const pubnub = usePubNub()
    const dispatch = useDispatch()
    const [messageValue, setMessageValue] = useState('')
    const [autocompleteItems, setAutoCompleteItems] = useState([])
    const [isOpenAutocomplete, setAutoComplete] = useState(false)
    const [selectionStart, setSelectionStart] = useState()

    useEffect(() => {
        let items = []
        if (dialoguesSettings !== null) {
            dialoguesSettings.phrases.map((item, index) =>
                items.push({ id: index, label: item })
            )
        }
        setAutoCompleteItems([...items])
    }, [dialoguesSettings])

    function useDebounce(value, delay) {
        const [debouncedValue, setDebouncedValue] = useState(value)

        useEffect(() => {
            const handler = setTimeout(() => {
                setDebouncedValue(value)
            }, delay)
            return () => {
                clearTimeout(handler)
            }
        }, [value])

        return debouncedValue
    }
    const handleSignal = (event) => {
        const message = event.message
        const publisher = event.publisher
        message === 'typing_on' && publisher === dialog.clientID
            ? dispatch(enableSignal())
            : dispatch(disableSignal())
    }

    const debouncedMessageInput = useDebounce(messageValue, 200)

    useEffect(() => {
        if (messageValue === '') {
            pubnub.signal(
                { message: 'typing_off', channel: channels[0] },
                () => {}
            )
            setAutoComplete(false)
        } else {
            setAutoComplete(true)
            pubnub.signal(
                { message: 'typing_on', channel: channels[0] },
                () => {}
            )
        }
    }, [debouncedMessageInput])

    const handleInput = (event) => {
        setSelectionStart(event.target.selectionStart)
        setMessageValue(event.target.value)
    }

    const handleEnableSignal = () => {
        pubnub.signal({ message: 'typing_on', channel: channels[0] }, () => {})
    }

    const handleDisableSignal = () => {
        pubnub.signal({ message: 'typing_off', channel: channels[0] }, () => {})
    }

    useEffect(() => {
        if (channels) {
            pubnub.addListener({ signal: handleSignal })
            pubnub.subscribe({ channels })
        }
        return () => {
            pubnub.removeAllListeners()
            pubnub.unsubscribeAll()
        }
    }, [pubnub, channels])

    return {
        dialog,
        messageValue,
        setMessageValue,
        selectionStart,
        setSelectionStart,
        autocompleteItems,
        setAutoCompleteItems,
        isOpenAutocomplete,
        setAutoComplete,
        handleInput,
        handleEnableSignal,
        handleDisableSignal,
    }
}

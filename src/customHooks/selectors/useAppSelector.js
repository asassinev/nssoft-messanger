export const appSelector = (store) => store.app
export const loadingSelector = (store) => store.app.loading
export const alertSelector = (store) => store.app.alert

export const curDialogSelector = (store) => store.dialog.dialog
export const dialogIdSelector = (store) => store.dialog.dialogId
export const dialogSelector = (store) => store.dialog

export const currentTabSelector = (store) => store.dialogues.currentTabItem
export const dbRefSelector = (store) => store.dialogues.dbRef
export const dialoguesSelector = (store) => store.dialogues

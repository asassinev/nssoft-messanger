export const curUserSelector = (store) => store.user.user
export const dialoguesSettingsSelector = (store) => store.user.dialoguesSettings
export const loadingSelector = (store) => store.user.loading
export const userSelector = (store) => store.user

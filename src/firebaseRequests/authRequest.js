import { firebaseApp, firebaseUrl } from '../firebaseConfig'

export const userLogin = async ({ email, password }) => {
    const { user } = await firebaseApp
        .auth()
        .signInWithEmailAndPassword(email, password)
    return user
}

export const loginWithSocial = async (provider) => {
    const { user } = await firebaseApp.auth().signInWithPopup(provider)

    await firebaseApp
        .database(firebaseUrl)
        .ref('usersConfig/' + user.uid)
        .once('value', (snapshot) => {
            if (snapshot.val() === undefined) {
                addUserProperty(user)
            } else {
                return
            }
        })
    await addUserProperty(user)

    return user
}

export const signUp = async (values) => {
    const { user } = await firebaseApp
        .auth()
        .createUserWithEmailAndPassword(values.email, values.password)
    await addUserProperty(user)
    return user
}

export const userLogout = async () => {
    await firebaseApp.auth().signOut()
}

// created userConfig for dialogues setting when created account
export const addUserProperty = (user) => {
    firebaseApp
        .database(firebaseUrl)
        .ref('usersConfig')
        .child(user.uid)
        .update({
            greeting: '',
            phrases: [''],
            listOfTopics: [''],
            listOfSubtopics: [''],
        })
}
export const forgotPassword = (email) =>
    firebaseApp.auth().sendPasswordResetEmail(email)

export const resetPassword = (values) =>
    firebaseApp.auth().confirmPasswordReset(values.oobCode, values.password)

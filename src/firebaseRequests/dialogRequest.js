import { firebaseApp, firebaseUrl } from '../firebaseConfig'

export const sendToActiveDialog = (action) => {
    let dialog = action.payload
    const ref = firebaseApp.database(firebaseUrl).ref('Waiting/' + dialog.id)
    ref.remove()
    delete dialog.id
    if (
        action.payload.greeting !== undefined &&
        action.payload.greeting !== ''
    ) {
        if (dialog.messages !== undefined) {
            dialog.messages.push({
                content: action.payload.greeting,
                timestamp: new Date().getTime(),
                writtenBy: 'operator',
            })
        } else {
            let messages = []
            messages.push({
                content: action.payload.greeting,
                timestamp: new Date().getTime(),
                writtenBy: 'operator',
            })
            dialog.messages = messages
        }
    }
    dialog.startedAt = new Date().getTime()
    delete dialog.greeting
    if (
        action.payload.greeting !== undefined &&
        action.payload.greeting !== ''
    ) {
        dialog.messages.push({
            content: action.payload.greeting,
            timestamp: new Date().getTime(),
            writtenBy: 'operator',
        })
    }

    if (dialog.remindMe) {
        let data = 'Ваш диалог начат'
        let headers = {
            'Content-Type': 'application/json; charset=utf-8',
            Authorization:
                'Basic NWIxNzQxOTctOWYwOC00OTAxLTgxNzgtYWUzNWI2YTM3MmVj',
        }

        let endpoint = 'https://onesignal.com/api/v1/notifications'

        let params = {
            method: 'POST',
            headers: headers,
            body: JSON.stringify({
                app_id: 'b432c026-de74-4864-be10-3fe5ae100ec7',
                contents: { en: data, ru: data },
                include_player_ids: [dialog.clientID],
            }),
        }
        fetch(endpoint, params)
    }

    delete dialog.greeting
    delete dialog.remindMe
    const dRef = firebaseApp.database(firebaseUrl).ref('Active')
    return dRef.push(dialog).key
}

export const saveDialog = async (currentDialog) => {
    const dRef = await firebaseApp.database(firebaseUrl).ref('Saved')
    let findId = null
    await dRef.once('value').then((snapshot) => {
        for (const id in snapshot.val()) {
            if (snapshot.val()[id].id === currentDialog.id) {
                findId = id
            }
        }
    })
    if (findId) {
        await dRef.child(findId).update(currentDialog)
    } else {
        delete currentDialog.id
        await dRef.push(currentDialog)
    }
}

export const deleteDialog = (action) => {
    firebaseApp
        .database(firebaseUrl)
        .ref('Saved/' + action.id)
        .remove()
}

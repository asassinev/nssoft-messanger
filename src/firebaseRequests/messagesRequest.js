import { firebaseApp, firebaseUrl } from '../firebaseConfig'

export const sendNewMessage = (action) => {
    const message = {
        content: action.payload.message,
        images: action.payload.images,
        timestamp: new Date().getTime(),
        writtenBy: 'operator',
    }
    if (!action.payload.dialog.messages) {
        const messages = []
        messages.push(message)
        action.payload.dialog.messages = messages
    } else {
        action.payload.dialog.messages.push(message)
    }
    const dRef = firebaseApp
        .database(firebaseUrl)
        .ref('Active')
        .child(action.payload.dialog.id)
    dRef.update({ messages: action.payload.dialog.messages })
}

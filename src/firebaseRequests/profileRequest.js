import { firebaseApp, firebaseUrl } from '../firebaseConfig'

export const userUpdateProfileFirebase = async ({ values, image }) => {
    //  upload photo
    const storageRef = firebaseApp.storage().ref()
    let photoURL = values.photoURL

    if (image !== null && image !== undefined) {
        await storageRef.child(values.email).putString(image, 'data_url')

        await storageRef
            .child(values.email)
            .getDownloadURL()
            .then((url) => {
                photoURL = url
            })
    }

    //update user profile
    let userNow = firebaseApp.auth().currentUser

    await userNow.updateProfile({
        displayName: values.name,
        photoURL: photoURL,
    })

    await userNow.updateEmail(values.email)

    if (values.password !== '') {
        await userNow.updatePassword(values.password)
    }

    return userNow
}

export const userUpdateDialoguesSettingsFirebase = ({ values, uid }) => {
    firebaseApp
        .database(firebaseUrl)
        .ref('usersConfig')
        .child(uid)
        .update(values)
}

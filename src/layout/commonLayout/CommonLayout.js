import React from 'react'
import { Spin } from 'antd'
import { useSelector } from 'react-redux'

import { loadingSelector } from '../../customHooks/selectors/useUserSelector'
import styles from './CommonLayout.module.scss'

export const CommonLayout = ({ children }) => {
    const loading = useSelector(loadingSelector)

    return (
        <div className={styles.container}>
            <h1 className={styles.title}>NSoft-Messenger</h1>
            <Spin spinning={loading} tip="Loading...">
                {children}
            </Spin>
        </div>
    )
}

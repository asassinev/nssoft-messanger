import React, { useRef, useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import debounce from 'lodash.debounce'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faSearch } from '@fortawesome/free-solid-svg-icons'
import { Input, InputGroup, InputGroupText } from 'reactstrap'

import { DropDown, DialoguesTabs } from '../../components/common'
import { setSearchValue } from '../../pages/Chats/effects/dialogs/action'
import { currentTabSelector } from '../../customHooks/selectors/useDialoguesSelector'
import styles from './LeftSideBar.module.scss'

export const LeftSideBar = () => {
    const [searchQuery, setSearchQuery] = useState('')
    const currentTabItem = useSelector(currentTabSelector)
    const dispatch = useDispatch()

    const delayQuery = useRef(
        debounce((e) => dispatch(setSearchValue({ searchValue: e.value })), 600)
    ).current

    const handleInput = (e) => {
        setSearchQuery(e.target.value)
        delayQuery({ value: e.target.value, currentTabItem })
    }

    return (
        <div className={styles.container}>
            <div className={styles.header}>
                <h5>NSoft-messenger</h5>
                <DropDown />
                <div className={styles.inputSearch}>
                    <InputGroup>
                        <InputGroupText>
                            <FontAwesomeIcon icon={faSearch} />
                        </InputGroupText>
                        <Input
                            type="text"
                            name="search"
                            id="floatingSearch"
                            placeholder="Search here..."
                            onChange={handleInput}
                            value={searchQuery}
                        />
                    </InputGroup>
                </div>
            </div>

            <DialoguesTabs />
        </div>
    )
}

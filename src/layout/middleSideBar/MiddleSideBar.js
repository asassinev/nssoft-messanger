import React, { useState } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faDoorOpen } from '@fortawesome/free-solid-svg-icons'

import {
    MessageInput,
    Messages,
    TopUserInfoPanel,
    UserInfoPanel,
} from '../../components/common'
import { Button } from '../../components/hoc'
import { resetDbRef } from '../../pages/Chats/effects/dialogs/action'
import { setToActiveDialog } from '../../pages/Chats/effects/dialog/action'
import { userSelector } from '../../customHooks/selectors/useUserSelector'
import { curDialogSelector } from '../../customHooks/selectors/useDialogSelector'
import { dialoguesSelector } from '../../customHooks/selectors/useDialoguesSelector'
import { typingSelector } from '../../customHooks/selectors/useMessagesSelector'
import styles from './MiddleSideBar.module.scss'

export const MiddleSideBar = () => {
    const [openUserInfo, setOpenUserInfo] = useState(false)
    const dialog = useSelector(curDialogSelector)
    const { user, dialoguesSettings } = useSelector(userSelector)
    const { currentTabItem, dbRef } = useSelector(dialoguesSelector)
    const typing = useSelector(typingSelector)
    const dispatch = useDispatch()

    const pushToActive = () => {
        dbRef.off()
        dispatch(resetDbRef())
        dispatch(
            setToActiveDialog({
                greeting: dialoguesSettings.greeting,
                operatorID: user.uid,
                operatorImg: user.photoURL || null,
                operatorName: user.displayName || user.email,
                ...dialog,
            })
        )
    }

    return (
        <div className={styles.container}>
            {dialog && (
                <>
                    <div className={styles.mainSection}>
                        <TopUserInfoPanel
                            setOpenUserInfo={() =>
                                setOpenUserInfo(!openUserInfo)
                            }
                        />
                        <Messages />
                        {typing && (
                            <p
                                style={{
                                    position: 'absolute',
                                    bottom: '66px',
                                    left: '6px',
                                }}
                            >
                                Печатает...
                            </p>
                        )}
                        {currentTabItem === 'Active' && <MessageInput />}
                        {currentTabItem === 'Waiting' && (
                            <Button
                                className={styles.enterDialog}
                                onClick={pushToActive}
                            >
                                <FontAwesomeIcon icon={faDoorOpen} /> Войти в
                                диалог
                            </Button>
                        )}
                    </div>
                    <UserInfoPanel
                        openUserInfo={openUserInfo}
                        onClose={() => setOpenUserInfo(false)}
                    />
                </>
            )}
        </div>
    )
}

import React, { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { PubNubProvider } from 'pubnub-react'
import PubNub from 'pubnub'

import { LeftSideBar } from '../../layout'
import { MiddleSideBar } from '../../layout'
import { firebaseApp, firebaseUrl } from '../../firebaseConfig'
import { setUserDialoguesSettings } from '../Login/effects/actions'
import { curUserSelector } from '../../customHooks/selectors/useUserSelector'
import styles from './Chats.module.scss'

export const Chats = () => {
    const user = useSelector(curUserSelector)
    const dispatch = useDispatch()
    const pubNub = new PubNub({
        publishKey: process.env.REACT_APP_PUBLISH_KEY_PUBNUB,
        subscribeKey: process.env.REACT_APP_SUBSCRIBE_KEY,
        uuid: user.uid,
    })

    useEffect(() => {
        const dRef = firebaseApp
            .database(firebaseUrl)
            .ref('usersConfig')
            .child(user.uid)
        dRef.on('value', (snapshot) => {
            dispatch(setUserDialoguesSettings(snapshot.val()))
        })
    }, [dispatch, user.uid])

    return (
        <div className={styles.container}>
            <PubNubProvider client={pubNub}>
                <LeftSideBar /> <MiddleSideBar />
            </PubNubProvider>
        </div>
    )
}

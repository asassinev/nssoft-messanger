import {
    CLEAR_CURRENT_DIALOG,
    CLEAR_DIALOG_ID,
    DELETE_DIALOG,
    DELETE_DIALOG_FAILURE,
    DELETE_DIALOG_REQUEST,
    DELETE_DIALOG_SUCCESS,
    SAVE_DIALOG,
    SAVE_DIALOG_FAILURE,
    SAVE_DIALOG_REQUEST,
    SAVE_DIALOG_SUCCESS,
    SET_CURRENT_DIALOG,
    SET_TO_ACTIVE_DIALOG,
    SET_TO_ACTIVE_DIALOG_FAILURE,
    SET_TO_ACTIVE_DIALOG_REQUEST,
    SET_TO_ACTIVE_DIALOG_SUCCESS,
} from './types'

export const setCurrentDialog = (dialog) => {
    return { type: SET_CURRENT_DIALOG, payload: dialog }
}

export const clearCurrentDialog = () => {
    return { type: CLEAR_CURRENT_DIALOG }
}

export const saveDialog = (currentDialog) => {
    return { type: SAVE_DIALOG, payload: currentDialog }
}

export const saveDialogRequest = () => {
    return { type: SAVE_DIALOG_REQUEST }
}

export const saveDialogSuccess = () => {
    return { type: SAVE_DIALOG_SUCCESS }
}

export const saveDialogFailure = () => {
    return { type: SAVE_DIALOG_FAILURE }
}

export const deleteDialog = (currentDialog) => {
    return { type: DELETE_DIALOG, payload: currentDialog }
}

export const deleteDialogRequest = () => {
    return { type: DELETE_DIALOG_REQUEST }
}

export const deleteDialogSuccess = () => {
    return { type: DELETE_DIALOG_SUCCESS }
}

export const deleteDialogFailure = () => {
    return { type: DELETE_DIALOG_FAILURE }
}

export const setToActiveDialog = (dialog) => {
    return { type: SET_TO_ACTIVE_DIALOG, payload: dialog }
}

export const setToActiveDialogRequest = () => {
    return { type: SET_TO_ACTIVE_DIALOG_REQUEST }
}

export const setToActiveDialogSuccess = (newId) => {
    return { type: SET_TO_ACTIVE_DIALOG_SUCCESS, payload: newId }
}

export const setToActiveDialogFailure = () => {
    return { type: SET_TO_ACTIVE_DIALOG_FAILURE }
}

export const clearDialogId = () => {
    return { type: CLEAR_DIALOG_ID }
}

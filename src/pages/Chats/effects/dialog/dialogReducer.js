import {
    CLEAR_CURRENT_DIALOG,
    SET_TO_ACTIVE_DIALOG_FAILURE,
    SET_TO_ACTIVE_DIALOG_REQUEST,
    SET_TO_ACTIVE_DIALOG_SUCCESS,
    DELETE_DIALOG_FAILURE,
    DELETE_DIALOG_REQUEST,
    DELETE_DIALOG_SUCCESS,
    SAVE_DIALOG_FAILURE,
    SAVE_DIALOG_REQUEST,
    SAVE_DIALOG_SUCCESS,
    SET_CURRENT_DIALOG,
    CLEAR_DIALOG_ID,
} from './types'

const initialState = {
    dialog: null,
    loading: false,
    hasMore: false,
    lastIndex: null,
    dialogId: null,
    channels: null,
}

export const dialogReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_CURRENT_DIALOG:
            return {
                ...state,
                loading: false,
                dialog: action.payload,
                lastIndex: null,
                channels: [action.payload.id],
            }
        case CLEAR_CURRENT_DIALOG:
            return {
                ...state,
                dialog: null,
                hasMore: false,
                lastIndex: null,
                channels: null,
            }
        case CLEAR_DIALOG_ID:
            return {
                ...state,
                dialogId: null,
            }
        case SET_TO_ACTIVE_DIALOG_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case SET_TO_ACTIVE_DIALOG_SUCCESS:
            return {
                ...state,
                loading: false,
                dialogId: action.payload,
            }
        case SET_TO_ACTIVE_DIALOG_FAILURE:
            return {
                ...state,
                loading: false,
            }
        case SAVE_DIALOG_REQUEST:
            return { ...state, loading: true }
        case SAVE_DIALOG_SUCCESS:
            return { ...state, loading: false }
        case SAVE_DIALOG_FAILURE:
            return { ...state, loading: false }
        case DELETE_DIALOG_REQUEST:
            return { ...state, loading: true }
        case DELETE_DIALOG_SUCCESS:
            return { ...state, loading: false }
        case DELETE_DIALOG_FAILURE:
            return { ...state, loading: false }
        default:
            return state
    }
}

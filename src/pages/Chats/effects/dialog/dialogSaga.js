import { call, put, takeLatest } from 'redux-saga/effects'
import { DELETE_DIALOG, SAVE_DIALOG, SET_TO_ACTIVE_DIALOG } from './types'
import {
    sendToActiveDialog,
    deleteDialog,
    saveDialog,
} from '../../../../firebaseRequests/dialogRequest'
import { showAlert } from '../../../../redux/actions'
import {
    deleteDialogFailure,
    deleteDialogRequest,
    deleteDialogSuccess,
    saveDialogFailure,
    saveDialogRequest,
    saveDialogSuccess,
    setToActiveDialogFailure,
    setToActiveDialogRequest,
    setToActiveDialogSuccess,
} from './action'
import { setCurrentTabItem } from '../dialogs/action'

export function* watcherDialog() {
    yield takeLatest(SAVE_DIALOG, asyncSaveDialog)
    yield takeLatest(SET_TO_ACTIVE_DIALOG, asyncSetToActiveDialog)
    yield takeLatest(DELETE_DIALOG, asyncDeleteDialog)
}

function* asyncSetToActiveDialog(action) {
    try {
        yield put(setToActiveDialogRequest())
        const newId = yield call(sendToActiveDialog, action)
        yield put(setCurrentTabItem('Active'))
        yield put(setToActiveDialogSuccess(newId))
        yield put(
            showAlert({ type: 'success', text: 'Добавлено в активные диалоги' })
        )
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(setToActiveDialogFailure())
    }
}

function* asyncSaveDialog(action) {
    try {
        yield put(saveDialogRequest())
        yield call(saveDialog, action.payload)
        yield put(saveDialogSuccess())
        yield put(showAlert({ type: 'success', text: 'Сохранено' }))
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(saveDialogFailure())
    }
}

function* asyncDeleteDialog(action) {
    try {
        yield put(deleteDialogRequest())
        yield call(deleteDialog, action.payload)
        yield put(deleteDialogSuccess())
        yield put(showAlert({ type: 'success', text: 'Удалено' }))
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(deleteDialogFailure())
    }
}

import {
    RESET_DB_REF,
    SET_CURRENT_TAB_ITEM,
    SET_DB_REF,
    SET_DIALOGUES,
    SET_SEARCH_VALUE,
} from './types'

export const setDialogues = (dialogues) => {
    return { type: SET_DIALOGUES, payload: dialogues }
}

export const setCurrentTabItem = (status) => {
    return { type: SET_CURRENT_TAB_ITEM, payload: status }
}

export const setSearchValue = (value) => {
    return { type: SET_SEARCH_VALUE, payload: value }
}

export const setDbRef = (ref) => {
    return { type: SET_DB_REF, payload: ref }
}

export const resetDbRef = () => {
    return { type: RESET_DB_REF }
}

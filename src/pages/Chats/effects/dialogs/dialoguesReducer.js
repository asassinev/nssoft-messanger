import {
    RESET_DB_REF,
    SET_CURRENT_TAB_ITEM,
    SET_DB_REF,
    SET_DIALOGUES,
    SET_SEARCH_VALUE,
} from './types'

const initialState = {
    dialogues: [],
    currentTabItem: 'Active',
    counterDialogues: 0,
    searchValue: '',
    dbRef: null,
}

export const dialoguesReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_DIALOGUES:
            return {
                ...state,
                loading: false,
                dialogues: action.payload,
            }
        case SET_SEARCH_VALUE:
            return {
                ...state,
                searchValue: action.payload.searchValue,
            }
        case SET_DB_REF:
            return { ...state, dbRef: action.payload }
        case RESET_DB_REF:
            return {
                ...state,
                dbRef: null,
            }
        case SET_CURRENT_TAB_ITEM:
            return { ...state, currentTabItem: action.payload }
        default:
            return state
    }
}

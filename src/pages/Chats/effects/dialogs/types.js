// Set dialogues
export const SET_DIALOGUES = 'SET_DIALOGUES'

// Set current dialogues type
export const SET_CURRENT_TAB_ITEM = 'SET_CURRENT_TAB_ITEM'

// set search value
export const SET_SEARCH_VALUE = 'SET_SEARCH_VALUE'

// set | reset DbRef dialogues
export const SET_DB_REF = 'SET_DB_REF'
export const RESET_DB_REF = 'RESET_DB_REF'

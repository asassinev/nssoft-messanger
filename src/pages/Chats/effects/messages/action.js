import {
    POST_NEW_MESSAGE,
    POST_NEW_MESSAGE_FAILURE,
    POST_NEW_MESSAGE_REQUEST,
    POST_NEW_MESSAGE_SUCCESS,
    SET_MESSAGES,
    ENABLE_SIGNAL,
    DISABLE_SIGNAL,
} from './types'

export const setMessages = (response) => {
    return {
        type: SET_MESSAGES,
        payload: {
            messages: response,
        },
    }
}

export const postNewMessage = (payload) => {
    return {
        type: POST_NEW_MESSAGE,
        payload: {
            message: payload.message,
            images: payload.images,
            dialog: payload.dialog,
            status: payload.status,
        },
    }
}

export const postNewMessageRequest = () => {
    return { type: POST_NEW_MESSAGE_REQUEST }
}

export const postNewMessageSuccess = () => {
    return { type: POST_NEW_MESSAGE_SUCCESS }
}

export const postNewMessageFailure = () => {
    return { type: POST_NEW_MESSAGE_FAILURE }
}

export const enableSignal = () => {
    return { type: ENABLE_SIGNAL }
}

export const disableSignal = () => {
    return { type: DISABLE_SIGNAL }
}

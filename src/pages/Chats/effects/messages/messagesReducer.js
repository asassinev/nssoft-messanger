import {
    POST_NEW_MESSAGE_FAILURE,
    POST_NEW_MESSAGE_REQUEST,
    POST_NEW_MESSAGE_SUCCESS,
    SET_MESSAGES,
    ENABLE_SIGNAL,
    DISABLE_SIGNAL,
} from './types'

const initialState = {
    messages: [],
    firstLoad: true,
    loading: false,
    typing: false,
}

export const messagesReducer = (state = initialState, action) => {
    switch (action.type) {
        case SET_MESSAGES:
            return {
                ...state,
                loading: false,
                messages: action.payload.messages,
                firstLoad: true,
            }
        case POST_NEW_MESSAGE_REQUEST:
            return { ...state, loading: true }
        case POST_NEW_MESSAGE_SUCCESS:
            return { ...state, loading: false }
        case POST_NEW_MESSAGE_FAILURE:
            return { ...state, loading: false }
        case ENABLE_SIGNAL:
            return {
                ...state,
                typing: true,
            }
        case DISABLE_SIGNAL:
            return {
                ...state,
                typing: false,
            }
        default:
            return state
    }
}

import { call, put, takeEvery } from 'redux-saga/effects'
import { sendNewMessage } from '../../../../firebaseRequests/messagesRequest'
import { showAlert } from '../../../../redux/actions'
import { POST_NEW_MESSAGE } from './types'
import {
    postNewMessageFailure,
    postNewMessageRequest,
    postNewMessageSuccess,
} from './action'

export function* watcherMessages() {
    yield takeEvery(POST_NEW_MESSAGE, asyncPostNewMessage)
}

function* asyncPostNewMessage(action) {
    try {
        yield put(postNewMessageRequest())
        yield call(() => sendNewMessage(action))
        yield put(postNewMessageSuccess())
    } catch (e) {
        yield put(showAlert(e.message))
        yield put(postNewMessageFailure())
    }
}

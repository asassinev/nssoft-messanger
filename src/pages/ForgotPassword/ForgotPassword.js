import React from 'react'

import { ForgotPassword as ForgotPasswordForm } from '../../components/forms/forgotPassword/ForgotPassword'
import { CommonLayout } from '../../layout/commonLayout/CommonLayout'

export const ForgotPassword = () => {
    return (
        <CommonLayout>
            <ForgotPasswordForm />
        </CommonLayout>
    )
}

import React from 'react'

import { Login as LoginForm } from '../../components/forms/login/Login'
import { CommonLayout } from '../../layout/commonLayout/CommonLayout'

export const Login = () => {
    return (
        <CommonLayout>
            <LoginForm />
        </CommonLayout>
    )
}

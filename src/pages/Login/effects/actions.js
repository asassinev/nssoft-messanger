import {
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_FAILURE,
    FORGOT_PASSWORD_REQUEST,
    FORGOT_PASSWORD_SUCCESS,
    RESET_PASSWORD,
    RESET_PASSWORD_FAILURE,
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_USER_UPDATED,
    SIGN_UP,
    SIGN_UP_FAILURE,
    SIGN_UP_REQUEST,
    SIGN_UP_SUCCESS,
    USER_LOGIN,
    USER_LOGIN_FAILURE,
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_WITH_SOCIAL,
    USER_LOGIN_WITH_SOCIAL_FAILURE,
    USER_LOGIN_WITH_SOCIAL_REQUEST,
    USER_LOGIN_WITH_SOCIAL_SUCCESS,
    USER_LOGOUT_REQUEST,
    USER_SET_DIALOGUES_SETTINGS,
    USER_UPDATE_DIALOGUES_SETTINGS,
    USER_UPDATE_DIALOGUES_SETTINGS_FAILURE,
    USER_UPDATE_DIALOGUES_SETTINGS_REQUEST,
    USER_UPDATE_DIALOGUES_SETTINGS_SUCCESS,
    USER_UPDATE_PROFILE,
    USER_UPDATE_PROFILE_FAILURE,
    USER_UPDATE_PROFILE_REQUEST,
    USER_UPDATE_PROFILE_SUCCESS,
} from './types'

export const userLogin = (values) => {
    return { type: USER_LOGIN, payload: values }
}

export const userLoginRequest = () => {
    return { type: USER_LOGIN_REQUEST }
}

export const userLoginSuccess = (user) => {
    return { type: USER_LOGIN_SUCCESS, payload: user }
}

export const userLoginFailure = () => {
    return { type: USER_LOGIN_FAILURE }
}

export const signUp = (values) => {
    return { type: SIGN_UP, payload: values }
}

export const signUpRequest = () => {
    return { type: SIGN_UP_REQUEST }
}

export const signUpSuccess = (user) => {
    return { type: SIGN_UP_SUCCESS, payload: user }
}

export const signUpFailure = () => {
    return { type: SIGN_UP_FAILURE }
}

export const userLoginWithSocial = (provider) => {
    return { type: USER_LOGIN_WITH_SOCIAL, payload: provider }
}

export const userLoginWithSocialRequest = () => {
    return { type: USER_LOGIN_WITH_SOCIAL_REQUEST }
}

export const userLoginWithSocialSuccess = (user) => {
    return { type: USER_LOGIN_WITH_SOCIAL_SUCCESS, payload: user }
}

export const userLoginWithSocialFailure = () => {
    return { type: USER_LOGIN_WITH_SOCIAL_FAILURE }
}

export const forgotPassword = (email) => {
    return { type: FORGOT_PASSWORD, payload: email }
}

export const forgotPasswordRequest = () => {
    return { type: FORGOT_PASSWORD_REQUEST }
}

export const forgotPasswordSuccess = () => {
    return { type: FORGOT_PASSWORD_SUCCESS }
}

export const forgotPasswordFailure = () => {
    return { type: FORGOT_PASSWORD_FAILURE }
}

export const resetPassword = (oobCode, password) => {
    return { type: RESET_PASSWORD, payload: { oobCode, password } }
}

export const resetPasswordRequest = () => {
    return { type: RESET_PASSWORD_REQUEST }
}

export const resetPasswordSuccess = () => {
    return { type: RESET_PASSWORD_SUCCESS }
}

export const resetPasswordFailure = () => {
    return { type: RESET_PASSWORD_FAILURE }
}

export const userLogout = () => {
    return { type: USER_LOGOUT_REQUEST }
}

export const userUpdateProfile = (values, image) => {
    return { type: USER_UPDATE_PROFILE, payload: { values, image } }
}

export const userUpdateProfileRequest = () => {
    return { type: USER_UPDATE_PROFILE_REQUEST }
}

export const userUpdateProfileSuccess = (user) => {
    return { type: USER_UPDATE_PROFILE_SUCCESS, payload: user }
}

export const userUpdateProfileFailure = () => {
    return { type: USER_UPDATE_PROFILE_FAILURE }
}

export const resetUserUpdated = () => {
    return { type: RESET_USER_UPDATED }
}

export const setUserDialoguesSettings = (settings) => {
    return { type: USER_SET_DIALOGUES_SETTINGS, payload: settings }
}

export const userUpdateDialoguesSettings = (values, uid) => {
    return { type: USER_UPDATE_DIALOGUES_SETTINGS, payload: { values, uid } }
}

export const userUpdateDialoguesSettingsRequest = () => {
    return { type: USER_UPDATE_DIALOGUES_SETTINGS_REQUEST }
}

export const userUpdateDialoguesSettingsSuccess = () => {
    return { type: USER_UPDATE_DIALOGUES_SETTINGS_SUCCESS }
}

export const userUpdateDialoguesSettingsFailure = () => {
    return { type: USER_UPDATE_DIALOGUES_SETTINGS_FAILURE }
}

import {
    FORGOT_PASSWORD_FAILURE,
    FORGOT_PASSWORD_REQUEST,
    FORGOT_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE,
    RESET_PASSWORD_REQUEST,
    RESET_PASSWORD_SUCCESS,
    RESET_USER_UPDATED,
    SIGN_UP_FAILURE,
    SIGN_UP_REQUEST,
    SIGN_UP_SUCCESS,
    USER_LOGIN_FAILURE,
    USER_LOGIN_REQUEST,
    USER_LOGIN_SUCCESS,
    USER_LOGIN_WITH_SOCIAL_FAILURE,
    USER_LOGIN_WITH_SOCIAL_REQUEST,
    USER_LOGIN_WITH_SOCIAL_SUCCESS,
    USER_LOGOUT_REQUEST,
    USER_SET_DIALOGUES_SETTINGS,
    USER_UPDATE_DIALOGUES_SETTINGS_FAILURE,
    USER_UPDATE_DIALOGUES_SETTINGS_REQUEST,
    USER_UPDATE_DIALOGUES_SETTINGS_SUCCESS,
    USER_UPDATE_PROFILE_FAILURE,
    USER_UPDATE_PROFILE_REQUEST,
    USER_UPDATE_PROFILE_SUCCESS,
} from './types'

const initialState = {
    loading: false,
    user: null,
    error: false,
    updated: false,
    dialoguesSettings: null,
}

export const userReducer = (state = initialState, action) => {
    switch (action.type) {
        case USER_LOGIN_REQUEST:
            return { ...state, loading: true }
        case USER_LOGIN_SUCCESS:
            return {
                ...state,
                loading: false,
                user: action.payload,
            }
        case USER_LOGIN_FAILURE:
            return {
                ...state,
                loading: false,
            }
        case SIGN_UP_REQUEST:
            return {
                ...state,
                loading: true,
            }
        case SIGN_UP_SUCCESS:
            return {
                ...state,
                user: action.payload,
                loading: false,
            }
        case SIGN_UP_FAILURE:
            return {
                ...state,
                loading: false,
            }
        case USER_LOGIN_WITH_SOCIAL_REQUEST:
            return { ...state, loading: true }
        case USER_LOGIN_WITH_SOCIAL_SUCCESS:
            return {
                ...state,
                loading: false,
                user: action.payload,
            }
        case USER_LOGIN_WITH_SOCIAL_FAILURE:
            return {
                ...state,
                loading: false,
            }

        case FORGOT_PASSWORD_REQUEST:
            return { ...state, loading: true }
        case FORGOT_PASSWORD_SUCCESS:
            return {
                ...state,
                loading: false,
            }
        case FORGOT_PASSWORD_FAILURE:
            return {
                ...state,
                loading: false,
            }
        case RESET_PASSWORD_REQUEST:
            return { ...state, loading: true }
        case RESET_PASSWORD_SUCCESS:
            return {
                ...state,
                loading: false,
            }
        case RESET_PASSWORD_FAILURE:
            return {
                ...state,
                loading: false,
            }
        case USER_LOGOUT_REQUEST:
            return {
                ...state,
                user: null,
            }
        case USER_UPDATE_PROFILE_REQUEST: {
            return { ...state, loading: true, updated: false, error: false }
        }
        case USER_UPDATE_PROFILE_SUCCESS: {
            return {
                ...state,
                loading: false,
                user: action.payload,
                error: false,
                updated: true,
            }
        }
        case USER_UPDATE_PROFILE_FAILURE: {
            return { ...state, loading: false, error: true }
        }
        case RESET_USER_UPDATED: {
            return { ...state, updated: false }
        }
        case USER_SET_DIALOGUES_SETTINGS:
            return {
                ...state,
                dialoguesSettings: action.payload,
            }
        case USER_UPDATE_DIALOGUES_SETTINGS_REQUEST: {
            return {
                ...state,
                loading: true,
                error: false,
            }
        }
        case USER_UPDATE_DIALOGUES_SETTINGS_SUCCESS: {
            return {
                ...state,
                loading: false,
                error: false,
            }
        }
        case USER_UPDATE_DIALOGUES_SETTINGS_FAILURE: {
            return {
                ...state,
                loading: false,
                error: true,
            }
        }
        default:
            return state
    }
}

import { call, put, takeLatest } from 'redux-saga/effects'
import {
    forgotPasswordFailure,
    forgotPasswordRequest,
    forgotPasswordSuccess,
    resetPasswordFailure,
    resetPasswordRequest,
    resetPasswordSuccess,
    signUpFailure,
    signUpRequest,
    signUpSuccess,
    userLoginFailure,
    userLoginRequest,
    userLoginSuccess,
    userLoginWithSocialFailure,
    userLoginWithSocialRequest,
    userLoginWithSocialSuccess,
    userUpdateDialoguesSettingsFailure,
    userUpdateDialoguesSettingsRequest,
    userUpdateDialoguesSettingsSuccess,
    userUpdateProfileFailure,
    userUpdateProfileRequest,
    userUpdateProfileSuccess,
} from './actions'
import {
    FORGOT_PASSWORD,
    RESET_PASSWORD,
    SIGN_UP,
    USER_LOGIN,
    USER_LOGIN_WITH_SOCIAL,
    USER_LOGOUT_REQUEST,
    USER_UPDATE_DIALOGUES_SETTINGS,
    USER_UPDATE_PROFILE,
} from './types'
import {
    forgotPassword,
    loginWithSocial,
    resetPassword,
    signUp,
    userLogin,
    userLogout,
} from '../../../firebaseRequests/authRequest'
import { showAlert } from '../../../redux/actions'
import {
    userUpdateDialoguesSettingsFirebase,
    userUpdateProfileFirebase,
} from '../../../firebaseRequests/profileRequest'

export function* watcherUserAuth() {
    yield takeLatest(USER_LOGIN, userAuth)
    yield takeLatest(SIGN_UP, signUpWorker)
    yield takeLatest(USER_LOGIN_WITH_SOCIAL, userAuthWithSocial)
    yield takeLatest(FORGOT_PASSWORD, forgotPasswordWorker)
    yield takeLatest(RESET_PASSWORD, resetPasswordWorker)
    yield takeLatest(USER_LOGOUT_REQUEST, userSignOut)
    yield takeLatest(USER_UPDATE_PROFILE, userUpdateProfileWorker)
    yield takeLatest(
        USER_UPDATE_DIALOGUES_SETTINGS,
        userUpdateDialoguesSettingsWorker
    )
}

function* userAuth(action) {
    try {
        yield put(userLoginRequest())
        const user = yield call(() => userLogin(action.payload))
        yield put(userLoginSuccess(user))
        yield put(
            showAlert({ type: 'success', text: 'Авторизация прошла успешно' })
        )
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(userLoginFailure())
    }
}

function* signUpWorker(action) {
    try {
        yield put(signUpRequest())
        const user = yield call(signUp, action.payload)
        yield put(signUpSuccess(user))
        yield put(
            showAlert({ type: 'success', text: 'Регистрация прошла успешно' })
        )
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(signUpFailure())
    }
}

function* userAuthWithSocial(action) {
    try {
        yield put(userLoginWithSocialRequest())
        const user = yield call(loginWithSocial, action.payload)
        yield put(userLoginWithSocialSuccess(user))
        yield put(
            showAlert({ type: 'success', text: 'Авторизация прошла успешно' })
        )
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
        yield put(userLoginWithSocialFailure())
    }
}

function* userSignOut() {
    try {
        yield call(() => userLogout())
        yield put(
            showAlert({ type: 'success', text: 'Вы успешно вышли из аккаунта' })
        )
    } catch (e) {
        yield put(showAlert({ type: 'error', text: e.message }))
    }
}

function* forgotPasswordWorker(action) {
    console.log(action.payload)
    try {
        yield put(forgotPasswordRequest())
        yield call(forgotPassword, action.payload)
        yield put(forgotPasswordSuccess())
        yield put(
            showAlert({
                type: 'success',
                text: 'Инструкция для восстановления пароля отправлена вам на почту',
            })
        )
    } catch (e) {
        yield put(forgotPasswordFailure())
        yield put(showAlert({ type: 'error', text: e.message }))
    }
}

function* resetPasswordWorker(action) {
    try {
        yield put(resetPasswordRequest())
        yield call(resetPassword, action.payload)
        yield put(resetPasswordSuccess())
        yield put(
            showAlert({
                type: 'success',
                text: 'Пароль успешно обновлен',
            })
        )
    } catch (e) {
        yield put(resetPasswordFailure())
        yield put(showAlert({ type: 'error', text: e.message }))
    }
}

function* userUpdateProfileWorker(action) {
    try {
        yield put(userUpdateProfileRequest())
        const user = yield call(userUpdateProfileFirebase, action.payload)
        yield put(userUpdateProfileSuccess(user))
        yield put(showAlert({ type: 'success', text: 'Успешно обновлено' }))
    } catch (e) {
        yield put(userUpdateProfileFailure())
        yield put(showAlert({ type: 'error', text: e.message }))
    }
}

function* userUpdateDialoguesSettingsWorker(action) {
    try {
        yield put(userUpdateDialoguesSettingsRequest())
        yield call(userUpdateDialoguesSettingsFirebase, action.payload)
        yield put(userUpdateDialoguesSettingsSuccess())
        yield put(showAlert({ type: 'success', text: 'Успешно обновлено' }))
    } catch (e) {
        yield put(userUpdateDialoguesSettingsFailure())
        yield put(showAlert({ type: 'error', text: e.message }))
    }
}

import React from 'react'

import { RestorePassword as RestorePasswordForm } from '../../components/forms/restorePassword/RestorePassword'
import { CommonLayout } from '../../layout/commonLayout/CommonLayout'

export const RestorePassword = () => {
    return (
        <CommonLayout>
            <RestorePasswordForm />
        </CommonLayout>
    )
}

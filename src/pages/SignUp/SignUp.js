import React from 'react'

import { SignUp as SignUpForm } from '../../components/forms/signUp/SignUp'
import { CommonLayout } from '../../layout/commonLayout/CommonLayout'

export const SignUp = () => {
    return (
        <CommonLayout>
            <SignUpForm />
        </CommonLayout>
    )
}

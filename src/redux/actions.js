import { SHOW_ALERT, HIDE_ALERT } from './types'

export const showAlert = (alert) => {
    return { type: SHOW_ALERT, payload: alert }
}

export const hideAlert = () => {
    return { type: HIDE_ALERT }
}

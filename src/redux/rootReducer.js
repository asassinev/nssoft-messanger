import storage from 'redux-persist/lib/storage'
import { combineReducers } from 'redux'
import { persistReducer } from 'redux-persist'
import { userReducer } from '../pages/Login/effects/userReducer'
import { appReducer } from './appReducer'
import { dialoguesReducer } from '../pages/Chats/effects/dialogs/dialoguesReducer'
import { dialogReducer } from '../pages/Chats/effects/dialog/dialogReducer'
import { messagesReducer } from '../pages/Chats/effects/messages/messagesReducer'

const persistConfig = {
    key: 'root',
    storage,
    whitelist: ['user', 'dialogues'],
}

export const rootReducer = persistReducer(
    persistConfig,
    combineReducers({
        user: userReducer,
        app: appReducer,
        dialogues: dialoguesReducer,
        dialog: dialogReducer,
        messages: messagesReducer,
    })
)

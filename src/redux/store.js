import createSagaMiddleware from 'redux-saga'
import { applyMiddleware, createStore } from 'redux'
import { rootReducer } from './rootReducer'
import { persistStore } from 'redux-persist'
import rootSaga from '../sagas'

const sagaMiddleware = createSagaMiddleware()
const store = createStore(rootReducer, applyMiddleware(sagaMiddleware))
const persistor = persistStore(store)
sagaMiddleware.run(rootSaga)

export { store, persistor }

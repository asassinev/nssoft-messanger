import { Chats } from './pages/Chats/Chats'
import { Login } from './pages/Login/Login'
import { SignUp } from './pages/SignUp/SignUp'
import { RestorePassword } from './pages/RestorePassword/RestorePassword'
import { ForgotPassword } from './pages/ForgotPassword/ForgotPassword'

export const publicRoutes = [
    {
        path: '/login',
        Component: Login,
    },
    {
        path: '/sign-up',
        Component: SignUp,
    },
    {
        path: '/restore-password',
        Component: RestorePassword,
    },
    {
        path: '/forgot-password',
        Component: ForgotPassword,
    },
]

export const privateRoutes = [
    {
        path: '/',
        Component: Chats,
    },
]

import { all } from 'redux-saga/effects'

import { watcherUserAuth } from '../pages/Login/effects/userSaga'
import { watcherDialog } from '../pages/Chats/effects/dialog/dialogSaga'
import { watcherMessages } from '../pages/Chats/effects/messages/messagesSaga'

export default function* rootSaga() {
    yield all([watcherUserAuth(), watcherDialog(), watcherMessages()])
}
